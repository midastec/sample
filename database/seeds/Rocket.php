<?php

use Illuminate\Database\Seeder;

class Rocket extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rocket')->insert([
            'model' => str_random(10),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
