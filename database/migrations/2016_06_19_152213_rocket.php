<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rocket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('rocket')) {
    //
            Schema::create('rocket', function (Blueprint $table) {
                 $table->increments('id');
                 //$table->primary('id');
                 $table->string('model');
                 $table->timestamps();
            });

        }   

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
   public function down()
    {
        Schema::drop('rocket');
    }
}
