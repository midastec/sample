<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 01/08/16
 * Time: 12:06 PM
 */

namespace App\Servicios\Evento;

use App\Servicios\Evento\Repository\EventoRepository;
use App\Servicios\Evento\Contracts\EventoServiceContract;

class EventoService implements EventoServiceContract{


	public function getAll($with = []){
		$eventos = new EventoRepository();
		return $eventos->all($with);
	}

	public function getCliente($evento_id ) {

		$evento = new EventoRepository($evento_id);
		$cliente = $evento->cliente();
	}

} 