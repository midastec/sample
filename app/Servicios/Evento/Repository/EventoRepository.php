<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 01/08/16
 * Time: 11:53 AM
 */
namespace App\Servicios\Evento\Repository;
use App\Models\Evento;
use App\Servicios\Evento\Repository\Contracts\EventoContract;


class EventoRepository implements EventoContract {

	private $id;
	private $evento;
	public function __construct( $evento_id = null ) {

	}

	public function all( $relations = []){
		$eventos = Evento::with($relations)->get()->toArray();
		return $eventos;
	}

	public function cliente($evento = null) {
		$evento = Evento::find($evento);
		return $evento->persona()->get()->toArray();
	}
} 