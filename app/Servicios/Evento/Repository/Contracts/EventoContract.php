<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 01/08/16
 * Time: 11:55 AM
 */

namespace App\Servicios\Evento\Repository\Contracts;


interface EventoContract {
	public function all( );

	public function cliente( );
} 