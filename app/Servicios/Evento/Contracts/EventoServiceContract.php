<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 01/08/16
 * Time: 02:07 PM
 */

namespace App\Servicios\Evento\Contracts;

interface EventoServiceContract {
	public function getAll();
	public function getCliente($evento);
}