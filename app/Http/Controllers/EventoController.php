<?php

namespace App\Http\Controllers;

use App\Servicios\Evento\Contracts\EventoServiceContract;
use App\Servicios\Evento\EventoService;
use Illuminate\Http\Request;

use App\Http\Requests;

class EventoController extends Controller
{
	public function index(EventoServiceContract $evento){
		//$evento = new EventoService();
		return $evento->getAll(['recursos','persona']);
    }
}
