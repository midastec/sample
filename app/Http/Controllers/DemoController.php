<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Helpers\Contracts\RocketShipContract;

class DemoController extends Controller
{
    
    public function index(RocketShipContract $rocketship)
	{
        $boom = $rocketship->blastOff();

        return view('index', compact('boom'));
	}
}
