<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 05/08/16
 * Time: 04:30 PM
 */

namespace App\Modules\Pdfreporter\Domain\Services\Contracts;


interface ReporterContract {
	function generateReport();
} 