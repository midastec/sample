<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 05/08/16
 * Time: 04:31 PM
 */

namespace App\Modules\Pdfreporter\Domain\Services;


use App\Modules\Pdfreporter\Domain\Services\Contracts\ReporterContract;
use App\Helpers\RocketShip;
class ReporterTCPDFService implements ReporterContract{
	public function generateReport(){
		$reporter = new RocketShip();
		return $reporter->blastOff();

	}
} 