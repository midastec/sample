<?php

namespace App\Modules\Productos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class CreateProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        "nombre" => 'required|string',
	        "descripcion" => 'required|string',
	        "costo_individual" => 'required|numeric',
	        "image_path" => 'string',
	        "usuario_id" => 'integer',
	        "tipo_producto" => 'integer',
	        'active' => 'string'
        ];
    }

	public function response(array $errors)
	{
		return response()->json(array('errors' => array(['code' => 422, 'message' => $errors])), 422);
	}
}
