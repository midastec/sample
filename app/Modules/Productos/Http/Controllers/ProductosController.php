<?php

namespace App\Modules\Productos\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Productos\Http\Requests\CreateProductoRequest;
use App\Modules\Productos\Domain\Services\ProductoService;
class ProductosController extends Controller
{

	public function store(CreateProductoRequest $request ) {
		$productoService = new ProductoService();
		$attr = [
			"nombre" => $request->nombre,
			"descripcion" => $request->descripcion,
			"costo_individual" => $request->costo_individual,
			"image_path" => $request->image_path,
			"usuario_id" => $request->usuario_id,
			"tipo_producto"=> $request->tipo_producto,
			'active' => $request->active,
		];
		return $productoService->create($attr);
	}

	public function index() {
		return 'MARIANNE ESPERRUJIDA';
	}
}
