<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 04/08/16
 * Time: 03:27 PM
 */

namespace App\Modules\Productos\Domain\Repositories\Contracts;


interface ProductosRepositoryInterface {
	public function getAll( );

	public function create(array $params = []);
} 