<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 05/08/16
 * Time: 02:11 PM
 */

namespace App\Modules\Productos\Domain\Repositories;
use App\Modules\Productos\Domain\Repositories\Contracts\ProductosRepositoryInterface;
use App\Modules\Productos\Domain\Entities\Producto;
class EloquentProductosRepository implements ProductosRepositoryInterface{

	public function getAll() {
			return Producto::all();
		}

	public function create(array $params = []){
		$producto = Producto::create($params)->toArray();
		return Producto::hydrate($producto);
	}
} 