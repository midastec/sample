<?php

namespace App\Modules\Productos\Domain\Entities;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
	protected $table = 'producto';

	public $timestamps = false;

	protected $fillable = [
		"nombre",
		"descripcion",
		"costo_individual",
		"image_path",
		"usuario_id",
		"tipo_producto",
		'active'
	];

	protected $hidden = [
		"created_at",
		"updated_at",
		"deleted_at",
	];

	/**
	 * Relación con modelo de Recursos
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function recurso()
	{
		return $this->hasMany('App\Models\Recursos');
	}

	public function tipo_producto()
	{
		return $this->belongsTo('App\Models\Category', 'tipo_producto');
	}

	public function GananciaCompartida()
	{
		return $this->belongsToMany('App\Models\Persona', 'ganancia_compartida', 'producto_id', 'socio_id')
		            ->withPivot(['porcentaje_socio', 'porcentaje_empresa', 'id']);
	}

	static function io_producto($request)
	{
		$id = $request->{0};
		$response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
		$code = 201;
		if (!empty($id)) {
			$producto = Producto::find($id);
			if ($producto) {
				$producto->active = ($producto->active == false ? true : false);
				$io_msg = ($producto->active == false ? 'Desactivo' : 'Activo');
				if ($producto->save()) {
					$response = array('success' => true, 'message' => "El registro se $io_msg exitosamente!");
					$code = 200;
				} else {
					$response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
					$code = 201;
				}
			} else {

				$response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
				$code = 201;
			}
		}

		return Response($response, $code);
	}

}
