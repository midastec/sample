<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 04/08/16
 * Time: 03:32 PM
 */

namespace App\Modules\Productos\Domain\Services\Contracts;


interface ProductoServiceContract {

	 function create( );

	 function PDFReportEventDispatcher();
}