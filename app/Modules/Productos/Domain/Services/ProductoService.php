<?php
/**
 * Created by Maykol Purica.
 * User: maykolpurica
 * Date: 05/08/16
 * Time: 02:31 PM
 */

namespace App\Modules\Productos\Domain\Services;


use App\Modules\Productos\Domain\Services\Contracts\ProductoServiceContract;
use App\Modules\Productos\Domain\Repositories\EloquentProductosRepository;
use App\Helpers\Contracts\RocketShipContract;
use App\Helpers\RocketShip;
class ProductoService implements ProductoServiceContract{
	private $productosRepo;
	public function __construct( ){
		$this->productosRepo = new EloquentProductosRepository();
	}
	public function create($columns = []){
		$producto = $this->productosRepo->create($columns);
		if($producto){
			$this->PDFReportEventDispatcher();
		}
		return $producto;

	}

	public  function PDFReportEventDispatcher(RocketShipContract $reporter) {
		$reporte = new RocketShip();

	}
} 