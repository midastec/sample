<?php

namespace App\Providers;

use App\Servicios\Evento\EventoService;
use Illuminate\Support\ServiceProvider;
class EventoServiceProvider extends ServiceProvider
{
	protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    $this->app->bind('App\Servicios\Evento\Contracts\EventoServiceContract', function(){
		    return new EventoService();
		    //return new RocketLauncher();

	    });
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['App\Servicios\Evento\Contracts\EventoServiceContract'];
	}
}
