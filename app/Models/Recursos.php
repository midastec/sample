<?php namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Recursos extends Model
{

    protected $table = 'public.recursos';

    public $timestamps = false;

    protected $fillable = [
        "descripcion",
        "costo_individual",
        "image_path",
        "es_interno",
        "socio_id",
        "producto_id",
        "codigo",
        "activo",
        "usuario_id",
        "persona_recurso_id",
        "vehiculo_recurso_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    /**
     * Relación con el modelo de Persona
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function persona()
    {
        return $this->belongsTo('App\Models\Persona', 'socio_id');
    }

    /**
     * Relación con el modelo de Persona
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function personaRecurso()
    {
        return $this->belongsTo('App\Models\Persona', 'persona_recurso_id');
    }

    /**
     * Relación con el modelo de Vehiculo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehiculoRecurso()
    {
        return $this->belongsTo('App\Models\Vehiculo', 'vehiculo_recurso_id');
    }

    /**
     * Relación con el modelo de Producto
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Relacion de Factura con Publicacion
     */
    public function evento_recurso()
    {
        return $this->hasMany('App\Models\EventoRecurso', 'recurso_id');
    }

    /**
     * Obtiene los recursos asociados en un producto.
     * @param integer $producto_contratado_id ID de producto contrato
     * @param bool $result inidica si retorna datos o la referecia de Eloquent
     * @return mixed
     */
    public function getRecursosByProduct($producto_contratado_id, $result = true)
    {
        $producto_id = ProductoContratado::where('id', '=', $producto_contratado_id)->pluck('producto_id');
        $recursos = self::whereHas('producto', function ($q) use ($producto_id) {
            return $q->where('producto_id', '=', $producto_id);
        });

        if($result){
            $recursos->get();
        }

        return $recursos;
    }

    public function evento()
    {
        return $this->belongsToMany('App\Models\Evento', 'eventos_recursos', 'recurso_id', 'evento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function recursos_categoria()
    {
        return $this->belongsToMany('App\Models\Category', 'public.recursos_categoria', 'recurso_id', 'category_id');
    }

    /**
     * Obtiene los recursos relacionados a un vehiculo de un socio en especifico.
     * @param array $params datos del evento asociado al recurso.
     * @author Jose Rodriguez
     * @return
     * @throws Exception
     */
    public function getChoferRelacionado($params)
    {
        $category_filter = ['_table' => 'recurso_category'];
        $vehiculo_asignado = $this->recursos_categoria()
            ->where($category_filter)
            ->get();

        if(empty($vehiculo_asignado)){
            throw new Exception('El vehiculo no tiene categoria que indique la cantidad de ruedas que posee.' . $this->id);
        }

        $choferes = Recursos
            ::whereHas('producto.tipo_producto', function($q){

                // Valor quemado xq es una categoria de negocios en la tabla category
                $filter_persona = ['category.id'=> 25];
                $q->where($filter_persona);
            })->whereHas('recursos_categoria',function($q) use ($vehiculo_asignado, $category_filter){
                $filters = [];
                foreach($vehiculo_asignado as $categoria){
                    array_push($filters,$categoria->id);

                    // Categoria Quemada que indica la cantidad de ruedas del vehiculo.
                    if($categoria->alt_value == 'ruedas'){
                        $category_filter['category.id'] = $categoria->id;
                    }
                }
                $q->where($category_filter);
                $q->whereIn('category.id',$filters);

            })->whereDoesntHave('evento', function($q) use ($params){
                $filters = [
                    "fecha_inicio" => $params['inicio'],
                    "fecha_fin" => $params['fin']
                ];
                $q->where($filters);
            })
            ->where('activo',true)
            ->whereNotIn('id', $params['seleccionados'])
            ->where('socio_id', $this->socio_id)
            ->first();

        // No hay choferes disponibles.
        if(empty($choferes)){
            return false;
        }

        // Cast to Array porque asi lo utiliza la asignación de recursos automatico.
        return $choferes->toArray();
    }

    /**
     * Obtiene los recursos disponibles para un evento, conocido el producto contrato y las fechas del evento.
     * @param array $params ['$producto_contratado_id' => 0, 'inicio' => fecha del evento, 'fin' => fecha del evento]
     * @return mixed
     * @author jose Rodriguez
     */
    public function disponibles($params)
    {
        $recursos = $this->getRecursosByProduct($params['producto_contratado_id'], false);
        $recursos1 = $recursos
            ->whereDoesntHave('evento_recurso.eventos', function($q) use ($params){
                $q->whereBetween('fecha_inicio',[$params['inicio'], $params['fin']]);
                $q->whereBetween('fecha_fin',[$params['inicio'], $params['fin']]);
            })
            ->where('activo',true)
            ->whereNotIn('id',$params['seleccionados'])
            ->get()
            ->toArray();

        $recursos = $this->getRecursosByProduct($params['producto_contratado_id'], false);
        $recursos2 = $recursos
            ->whereDoesntHave('evento_recurso')
            ->whereNotIn('id',$params['seleccionados'])
            ->where('activo',true)
            ->get()
            ->toArray();

        return array_merge($recursos1, $recursos2);
    }
}

// PostgreSQL y MySQL Conexiones externas. -> para receta.
// http://blog.jsinh.in/how-to-enable-remote-access-to-postgresql-database-server/#.Vt7rAULL9z0
// https://www.turnkeylinux.org/docs/database-remote-access

