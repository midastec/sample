<?php

namespace App\Models;

use App\Models\Acl\Group;
use App\Models\Acl\UserGroup;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;
use yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use yajra\Datatables\Helper;
use App\Models\Evento;

class Pagos extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.pagos';
    public $timestamps = false;
    protected $fillable = [
        'evento_id',
        'forma_pago',
        'banco',
        'transaccion',
        'fecha_pago',
        'usuario_id',
        'file_pago',
        'persona_id'
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\UserModel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function eventos()
    {
        return $this->belongsToMany('App\Models\Evento', 'public.evento_pago', 'pago_id', 'evento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function evento_pago()
    {
        return $this->belongsToMany('App\Models\Evento', 'public.evento_pago', 'pago_id', 'evento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function estatus_pago_simple()
    {
        return $this->hasMany('App\Models\PagosEstatus', 'pago_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function estatus_pago()
    {
        return $this->belongsToMany('App\Models\Category', 'public.pagos_estatus', 'pago_id', 'category_id');
    }

    /**
     * @return mixed
     */
    public function ultimo_estatus_pago()
    {
        return $this->estatus_pago()->orderby('public.pagos_estatus.id', 'desc');
    }

    /**
     * @return mixed
     */
    public function ultimo_estatus_pago_simple()
    {
        return $this->estatus_pago_simple()->orderby('public.pagos_estatus.id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function banco()
    {
        return $this->belongsTo('App\Models\Category', 'banco');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forma_pago()
    {
        return $this->belongsTo('App\Models\Category', 'forma_pago');
    }

    /**
     * @param null $params
     * @return Collection|static[]
     */
    static function pagos($filter = array())
    {
        $pago = Pagos::with([
            "ultimo_estatus_pago",
            "banco",
            "forma_pago",
            "evento_pago",
            "persona"
        ]);
        if (!empty($filter['persona_id']))
            $pago->where(array('persona_id' => $filter['persona_id']));

        $pago_array = $pago->get()->toArray();
        $collection = new Collection();

        foreach ($pago_array as $key => $value) {
            $value['estatus_pago_label'] = '';
            if (sizeof($value['ultimo_estatus_pago']) > 0)
                $value['estatus_pago_label'] = $value['ultimo_estatus_pago'][0]['_label'];

            if (!empty($filter['estatus'])):
                if (sizeof($value['ultimo_estatus_pago']) > 0):
                    $value['estatus_pago_label'] = $value['ultimo_estatus_pago'][0]['_label'];
                    if (in_array((int)$value['ultimo_estatus_pago'][0]['id'], array_fetch($filter['estatus'], 'id'))):
                        $collection->push($value);
                    endif;
                endif;
            else:
                $collection->push($value);
            endif;
        }
        return $collection;
    }

    static function detail_pago($id_pago, $solo_arreglo = false)
    {
        $datos = Pagos::with([
            "ultimo_estatus_pago",
            "banco",
            "forma_pago",
            "evento_pago",
            "persona",
            "forma_pago",
            "ultimo_estatus_pago_simple"
        ])->find($id_pago)->toArray();
        if ($solo_arreglo)
            return $datos;
        return response()->json($datos, 200);
    }

    static function emailNotificarPagoAdmin($evento, $pago)
    {

        $group=Group::where('name', 'Administrador')->get()->toArray();
        $userGroup=UserGroup::where('group_id', $group[0]['id'])->get();

        if($userGroup->isEmpty()){
            return response()->json(array('status' => 'fail', 'messages' => 'No se puede enviar el correo al administrador de la página.'), 500);
        }

        $user=UserModel::where('id', $userGroup[0]['user_id'])->get();

        $cliente=Persona::find($evento[0]['cliente_id']);
        $evento[0]['cliente']=$cliente->nombre." ".$cliente->apellido;

        $pago['forma_pago']=Category::find($pago['forma_pago'])->_label;
        $pago['banco']=Category::find($pago['banco'])->_label;

        $encabezado['from'] = array(
            'email' => 'no-responder@route-enjoy.com',
            'nombre' => 'Sistema Route Enjoy'
        );

        $encabezado['subject'] = 'Route Enjoy (Cambio en estatus de pago de evento '.$evento[0]['codigo'].').';
        $response = Mail::queue(
            'notificarPagoAdmin',
            array('encabezado' => $encabezado, 'evento' => $evento, 'pago' => $pago), function ($message) use ($encabezado, $evento, $pago, $user) {
            $message->to(Evento::getAdminEmail(), $user[0]->username);
            $message->from($encabezado['from']['email'], $encabezado['from']['nombre']);
            $message->subject($encabezado['subject']);
        });

        return $response;
    }

}
