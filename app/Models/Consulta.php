<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Goutte\Client;


class Consulta extends Model
{
    var $response;

    /**
     * @return array $this->response
     */
    public function getDataRNC($rif)
    {
        $client = new Client();
        try {
            $crawler = $client->request('GET', "http://rncenlinea.snc.gob.ve/reportes/resultado_busqueda?p=1&rif=$rif&search=RIF");
            $configurationRows = $crawler->filter('html table table tr');
            $this->response = [
                "rif" => "",
                "razon_social" => "",
                "estatus" => "",
                "nivel" => "",
                "contacto" => "",
                "telefonos" => "",
            ];
            $configurationRows->each(function ($configurationRow, $index) {
                if (intval($index) == 1 && count($configurationRow->filter('td')) > 2) {
                    $this->response = array(
                        'rif' => trim($configurationRow->filter('td')->eq(0)->text()),
                        'razon_social' => $configurationRow->filter('td')->eq(1)->text(),
                        'estatus' => $configurationRow->filter('td')->eq(2)->text(),
                        'nivel' => $configurationRow->filter('td')->eq(3)->text(),
                        'contacto' => $configurationRow->filter('td')->eq(4)->text(),
                        'telefonos' => $configurationRow->filter('td')->eq(5)->text(),
                    );
                }
            });
        } catch (\Exception $e) {
            $this->response = [
                "rif" => "",
                "razon_social" => "",
                "estatus" => "",
                "nivel" => "",
                "contacto" => "",
                "telefonos" => "",
                "status" => "fail",
            ];
        }
        if ($this->response['rif'] == null):
            $this->response['status'] = 'fail';
        else:
            $this->response['status'] = 'ok';
        endif;
        return $this->response;
    }

    public function getSolvencia($rif)
    {
        $client = new Client();
        try {
            $crawler = $client->request('POST', "http://app.gestion.minpptrass.gob.ve/gestion/mod_herramientas/reporte_ultima_solvencia.php", array('btnAceptar' => 'Buscar', 'rif' => $rif));

            $configurationRows = $crawler->filter('html table');

            $this->response = null;
            $configurationRows->each(function ($configurationRow, $index) {
                if (intval($index) == 1) {
                    $configurationRow2 = $configurationRow->filter('tr');
                    $configurationRow2->each(function ($configurationRow3, $index2) {

                        if (intval($index2) == 3) {
                            if (!empty($configurationRow3))
                                $this->response = array(
                                    'rif' => $configurationRow3->filter('td')->eq(0)->text(),
                                    'razon_social' => $configurationRow3->filter('td')->eq(1)->text(),
                                    'estatus' => $configurationRow3->filter('td')->eq(2)->text(),
                                    'ultima_solicitud' => $configurationRow3->filter('td')->eq(3)->text(),

                                );
                            if ($this->response['rif'] == "" || $this->response['razon_social'] == "" || $this->response['estatus'] == "" || $this->response['ultima_solicitud'] == ""):
                                $this->response['status'] = 'fail';
                            else:
                                $this->response['status'] = 'ok';
                            endif;
                        }
                    });

                }
            });
        }catch (\Exception $e){
            $this->response = array(
                'rif' => "",
                'razon_social' => "",
                'estatus' => "",
                'ultima_solicitud' => "",
                'status' => "fail",

            );
        }
        return $this->response;
    }

    public function getSeniat($rif)
    {
        try {
            $url = "http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=$rif";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);
            $this->response = [
                "nombre" => "",
                "agenteretencioniva" => "",
                "contribuyenteiva" => "",
                "tasa" => "",
                "status" => 'fail'
            ];

            if ($result) {
                try {
                    if (substr($result, 0, 1) != '<') {
                        return [
                            "nombre" => "",
                            "agenteretencioniva" => "",
                            "contribuyenteiva" => "",
                            "tasa" => "",
                            "status" => 'fail'
                        ];
                    }
                    $xml = simplexml_load_string($result);
                    if (!is_bool($xml)) {
                        $elements = $xml->children('rif');
                        $seniat = array();
                        $responseJson['code_result'] = 1;
                        $responseJson['message'] = 'Consulta satisfactoria';
                        $this->response = array();
                        foreach ($elements as $key => $node) {
                            $index = strtolower($node->getName());
                            $this->response[$index] = (string)$node;
                        }
                        $this->response['status'] = 'ok';
                        $responseJson['seniat'] = $seniat;
                    }
                } catch (Exception $e) {
                    $exception = explode(' ', $result, 2);
                    $responseJson['code_result'] = (int)$exception[0];
                }
            } else {
                // No hay conexión a internet
                $responseJson['code_result'] = 0;
                $responseJson['message'] = 'Sin Conexión a Internet';
            }

            $this->response = $this->processSeniatData($this->response);
        }catch (\Exception $e){
           $this->response = [
                "nombre" => "",
                "agenteretencioniva" => "",
                "contribuyenteiva" => "",
                "tasa" => "",
                "status" => 'fail'
            ];
        }
        return $this->response;
    }

    /**
     * Elimina la información que viene luego del parentesis de los servicios de SENIAT.
     *
     * @param $data
     * @return mixed
     * @author  Jose Rodriguez
     */
    private function processSeniatData($data)
    {
        $data['nombre'] = substr($data['nombre'], 0, strrpos($data['nombre'], '('));
        $data['nombre'] = trim($data['nombre']);
        return $data;
    }

}
