<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Acl\Group;
use Illuminate\Support\Facades\Validator;
use yajra\Datatables\Datatables;
use App\Models\UserModel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Category;

class Persona extends Model
{

    protected $table = 'persona';
    public $timestamps = false;

    protected $fillable = [
        'nombre',
        'apellido',
        'ci',
        'email',
        'telefono',
        'id',
        'rif',
        'tipo_persona_id',
        'image_path',
        'active'
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    /**
     * Relación con modelo de Recursos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recurso()
    {
        return $this->hasMany('App\Models\Recursos');
    }

    /**
     * Relación con modelo de Contactos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactos()
    {
        return $this->hasMany('App\Models\Contactos');
    }

    /**
     * Relación con modelo de Evento
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evento()
    {
        return $this->hasMany('App\Models\Evento', 'cliente_id');
    }

    /**
     * Relación con modelo de Usuario
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\UserModel');
    }

    public function personaUser()
    {
        return $this->hasMany('App\Models\UserModel', 'persona_id');
    }

    public function tipoPersona()
    {
        return $this->belongsTo('App\Models\Category')
            ->where('_table', 'tipo_persona');
    }

    public function getDates()
    {
        return array();
    }

    public function GananciaCompartida()
    {
        return $this->belongsToMany('App\Models\Producto', 'ganancia_compartida', 'socio_id', 'producto_id')
            ->withPivot(['porcentaje_socio', 'porcentaje_empresa', 'id']);
    }

    /* public function GananciaCompartidaBy(){

     }*/
    static function dt_persona($request = null)
    {
        $category_cliente_id = array();
        $cliente = new Collection();
        if (!is_null($request->tipo_persona)):
            if (is_array($request->tipo_persona)):
                foreach ($request->tipo_persona as $key => $value):
                    $category_persona_id[] = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', $value))->pluck('id');
                endforeach;
            else:
                $category_persona_id[] = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', $request->tipo_persona))->pluck('id');
            endif;
            $datos = Persona::whereIn('tipo_persona_id', $category_persona_id)->get();
        else:
            $datos = Persona::all();
        endif;

        $category_cliente_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', 'cliente'))->pluck('id');


        $datos = Persona::where('tipo_persona_id', '=', $category_persona_id)->get();
        $user_object = new UserModel();
        if (!empty($datos)) {
            $data = $user_object->encript_decrypt_datos(
                $datos->toArray(), 'decrypt', array(
                'nombre',
                'apellido',
                'telefono',
                'ci',
                'email',
                'rif'
            ), true
            );
            for ($i = 0; $i < count($data); $i++) {
                $cliente->push([
                    'id' => $data[$i]['id'],
                    'nombre' => $data[$i]['nombre'],
                    'apellido' => $data[$i]['apellido'],
                    'ci' => $data[$i]['ci'],
                    'telefono' => $data[$i]['telefono'],
                    'email' => $data[$i]['email'],
                    'rif' => $data[$i]['rif'],
                    'active' => $data[$i]['active'],
                ]);
            }
        }
        return Datatables::of($cliente)->make(true);
    }

    function get_persona($id)
    {
        $datos = Persona::where('id', '=', $id)->get();
        $user_object = new UserModel();
        if (!empty($datos)) {
            $data = $user_object->encript_decrypt_datos(
                $datos->toArray(), 'decrypt', array(
                'nombre',
                'apellido',
                'telefono',
                'ci',
                'email',
                'rif'
            ), true
            );
        }

        return response()->json(array('status' => 'ok', 'data' => (sizeof($data) > 0 ? $data[0] : array())), 200);
    }

    static function listado_socios($tipo_persona)
    {

        switch ($tipo_persona) {

            case 'interno':
                $category_cliente_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', 'interno'))->pluck('id');
                $datos = Persona::where('tipo_persona_id', '=', $category_cliente_id)->get()->toArray();
                return $datos;

                break;
            case 'cliente':
                $category_cliente_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', 'cliente'))->pluck('id');
                $datos = Persona::where('tipo_persona_id', '=', $category_cliente_id)->get()->toArray();
                return $datos;

                break;
            case 'socio':
                $category_cliente_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', 'socio'))->pluck('id');
                $datos = Persona::where('tipo_persona_id', '=', $category_cliente_id)->
                where('active', '=', true)->get()->toArray();
                return $datos;

                break;
            case 'recurso':
                $category_cliente_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', 'recurso'))->pluck('id');
                $datos = Persona::where('tipo_persona_id', '=', $category_cliente_id)->get()->toArray();
                return $datos;

                break;
        }
    }

    static function create_persona($request)
    {
        try {
            $category_persona_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', $request->tipo_persona))->pluck('id');

            $personaData = [
                'nombre' => $request->input('first_name'),
                'apellido' => $request->input('last_name'),
                'ci' => $request->input('cedula'),
                'email' => $request->input('email'),
                'telefono' => $request->input('phone'),
                'tipo_persona_id' => $category_persona_id,
                'rif' => ($request->has('rif') ? substr(str_replace(' ', '', $request->input('rif')), 0, 9) : null)

            ];
            $v = Validator::make($personaData, [
                'nombre' => 'required',
                'apellido' => 'required',
                'ci' => 'required',
                'email' => 'required',
                'telefono' => 'required',
                'tipo_persona_id' => 'required',
            ]);
            if ($v->fails()) {
                $response = array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()]));
                $code = 422;
                return array('response' => $response, 'code' => $code);
            }

            $userData = $request;
            $userCreate = UserModel::create_usuario($userData, true, true);

            if (preg_match("/422|202|201/i", $userCreate['code'])) {
                return $userCreate;
            }

            return $userCreate;

        } catch (Exception $e) {
            $response = array('success' => false, 'messages' => 'Error');
            $code = HttpResponse::HTTP_CONFLICT;
            return array('response' => $response, 'code' => $code);

        }

    }


    static function editar_persona($idPersona, $request)
    {
//        \DB::beginTransaction();
        try {
            $category_persona_id = Category::WhereRaw('_table = ? AND _label = ?', array('tipo_persona', $request->tipo_persona))->pluck('id');

            $personaData = [
                'nombre' => $request->input('first_name'),
                'apellido' => $request->input('last_name'),
                'ci' => $request->input('cedula'),
                'email' => $request->input('email'),
                'telefono' => $request->input('phone'),
                'tipo_persona_id' => $category_persona_id,
                'rif' => ($request->has('rif') ? substr(str_replace(" ", "", $request->input('rif')), 0, 9) : null)

            ];
            $v = Validator::make($personaData, [
                'nombre' => 'required',
                'apellido' => 'required',
                'ci' => 'required',
                'email' => 'required',
                'telefono' => 'required',
                'tipo_persona_id' => 'required',
            ]);
            if ($v->fails()) {
//                DB::rollback();
                $response = array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()]));
                $code = 422;
                return array('response' => $response, 'code' => $code);
            }
            $persona_object = new Persona();
            $user_object = new UserModel();
            $data_user = $user_object->where('persona_id', '=', $idPersona)->get()->toArray();
            $idUsuario = (sizeof($data_user) > 0 ? $data_user[0]['id'] : null);
            $persona = $persona_object->find($idPersona);
            if ($persona):
                //Encriptacion de datos de usuario
                $data = $user_object->encript_decrypt_datos(
                    $personaData, //Datos a procesar
                    'encrypt',//Modo que aplicara
                    array(
                        'nombre',
                        'apellido',
                        'ci',
                        'email',
                        'telefono'
                    )//Claves que se le aplicara encriptacion
                );
                foreach ($data as $key => $value):
                    $persona->{$key} = $data[$key];
                endforeach;
                if ($persona->save()):
                    $data_usuario = array(
                        'first_name' => $personaData['nombre'],
                        'last_name' => $personaData['apellido'],
                        'email' => $personaData['email'],
                        'phone' => $personaData['telefono'],
                        'cedula' => $personaData['ci'],
                        'direccion' => '',
                        'id' => $idPersona
                    );

                    if (!empty($idUsuario))
                        $update_user = $user_object->update_usuario($idUsuario, (object)$data_usuario, false);
                    $response = array('success' => true, 'messages' => 'Registro actualizado exitosamente!');
                    $code = 200;
                    return array('response' => $response, 'code' => $code);
                endif;
            endif;
            $response = array('success' => false, 'messages' => 'Error al crear el registro!');
            $code = 201;
            return array('response' => $response, 'code' => $code);
        } catch (Exception $e) {
            $response = array('success' => false, 'messages' => 'Error');
            $code = HttpResponse::HTTP_CONFLICT;
            return array('response' => $response, 'code' => $code);

        }

    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $value
     * @return mixed
     */
    public function getNombreAttribute($value)
    {
        if (!$this->invalidPayload($value)):
            return Crypt::decrypt($value);
        else:
            return $value;
        endif;
    }

    public function getApellidoAttribute($value)
    {
        if (!$this->invalidPayload($value)):
            return Crypt::decrypt($value);
        else:
            return $value;
        endif;
    }

    public function getEmailAttribute($value)
    {
        if (!$this->invalidPayload($value)):
            return Crypt::decrypt($value);
        else:
            return $value;
        endif;
    }

    public function getCiAttribute($value)
    {
        if (!$this->invalidPayload($value)):
            return Crypt::decrypt($value);
        else:
            return $value;
        endif;
    }

    public function getTelefonoAttribute($value)
    {
        if (!$this->invalidPayload($value)):
            return Crypt::decrypt($value);
        else:
            return $value;
        endif;
    }

    public function getRifAttribute($value)
    {
        if (!$this->invalidPayload($value)):
            return Crypt::decrypt($value);
        else:
            return $value;
        endif;
    }

    public function invalidPayload($payload)
    {
        if (is_string($payload)):
            $data = json_decode(base64_decode($payload), true);
            return !is_array($data) || !isset($data['iv']) || !isset($data['value']) || !isset($data['mac']);
        else:
            return true;
        endif;
    }

    static function io_persona($request)
    {
        $id = $request->{0};
        $response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
        $code = 201;
        if (!empty($id)) {
            $persona = Persona::find($id);
            if ($persona) {
                $persona->active = ($persona->active == false ? true : false);
                $io_msg = ($persona->active == false ? 'Desactivo' : 'Activo');
                if ($persona->save()) {
                    $response = array('success' => true, 'message' => "El registro se $io_msg exitosamente!");
                    $code = 200;
                } else {
                    $response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
                    $code = 201;
                }
            } else {

                $response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
                $code = 201;
            }
        }

        return Response($response, $code);
    }
}
