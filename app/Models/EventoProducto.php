<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventoProducto extends Model
{
    protected $table = 'public.evento_producto';

    public $timestamps = false;

    protected $fillable = [
        "evento_id",
        "producto_contratado_id",
        "cantidad"
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Relación con el modelo de ProductoContratado
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto_contratado()
    {
        return $this->belongsTo('App\Models\ProductoContratado', 'producto_contratado_id');
    }

}
