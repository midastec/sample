<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'pais';

    protected $fillable = [
        "nombre",
        "usuario_id"
    ];

    protected $hidden = [

        "created_at",
        "updated_at"
    ];

    /**
     * Relación con el modelo Estado
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function estado ()
    {
        return $this->hasMany ('App\Models\Estado');
    }
}
