<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Maykol Purica <puricamaykol@gmail.com>
 *          Class Contrato
 * @package App\Models
 */
class Contrato extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'contratos';

    public $timestamps = false;

    protected $fillable = [
        "cliente_id",
        "es_postpago",
        "tarifa",
        "regimen_pago",
        "cuota",
        "cerrado",
        "activo",
        'firmante_nombre',
        'firmante_cargo',
        'codigo',
        "usuario_id",
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    /**
     * Relación con modelo de Cliente
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente ()
    {
        return $this->belongsTo ('App\Models\Cliente');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balance ()
    {
        return $this->hasMany ('App\Models\Balance');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     * Esta funcion se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacio entonces ninguno lo sera
     */
    public function getDates ()
    {
        return array();
    }
}
