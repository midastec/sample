<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "category";
    
    public $timestamps = false;
      // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        '_table',
        '_label',
        'alt_value',
        '_order',
        
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category_category()
    {
        return $this->belongsToMany('App\Models\Category', 'public.category_category', 'id_parent', 'id_child');
    }
	static function getByAltVal($alt_val){
		return self::where('alt_value', '=', $alt_val)->pluck('id');
	}
	static function getByNotAltVal($alt_val){
		$res = self::where('alt_value', '<>', $alt_val)->where('_table', '=', 'estatus_evento')->get(['id'])->toArray();
		//dd( array_values((array_values($res))));
		$res_long = count($res);
		for($i = 0; $i <$res_long; $i++){
			$out[] = $res[$i]['id'];
		}
		return $out;
	}
    
    
    

}
