<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Exception;
use \HttpResponse;
use App\Models\Recursos;
use App\Models\UserModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection as Collection;
use App\Models\Lapso;
use App\Models\Email;
use App\Models\Comment;

class Evento extends Model {
	private $attach_mail = null;
	private $data_email = null;
	private $all_emails = null;
	private $data_cliente = null;
	private $PdfCotizacionTemplate = 'PdfCotizacion';

	protected $table = 'evento';
	public $timestamps = false;
	protected $fillable = [
		'cliente_id',
		'fecha_inicio',
		'fecha_fin',
		'iva',
		'montoiva',
		'total',
		'totaliva',
		'clave_aleatoria',
		'alias'
	];

	protected $hidden = [
		/* "created_at",
		 "updated_at",
		 "deleted_at",*/
	];

	public function getDates() {
		return array();
	}

	public function recursos() {
		return $this->belongsToMany( 'App\Models\Recursos', 'eventos_recursos', 'evento_id', 'recurso_id' )
		            ->withPivot( 'porcentaje_socio' );
	}

	public function productosContratados() {
		return $this->belongsToMany( 'App\Models\ProductoContratado', 'evento_producto', 'evento_id', 'producto_contratado_id' )
		            ->withPivot( 'cantidad' );
	}

	public function evento_producto() {
		return $this->hasMany( 'App\Models\EventoProducto', 'evento_id' );
	}

	public function evento_recurso() {
		return $this->hasMany( 'App\Models\EventoRecurso', 'evento_id' );
	}


//=============================================================
	/*    public function rates(){
			return $this->belongsToMany('App\Models\Role'); // el segundo parametro es el nombre de la tabla pero como cumplimos la convencion no hace falta
		}*/

	public function comments() {
		return $this->hasMany( 'App\Models\Comment' );
	}


	//=============================================================

	public function persona() {
		return $this->belongsTo( 'App\Models\Persona', 'cliente_id' );
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function estatus_evento() {
		return $this->belongsToMany( 'App\Models\Category', 'public.evento_estatus', 'evento_id', 'category_id' );
	}

	/**
	 * @return mixed
	 */
	public function ultimo_estatus_evento() {
		//eturn $this->estatus_evento()->orderby('evento_estatus.id', 'desc');
		return $this->belongsToMany( 'App\Models\Category', 'public.evento_estatus', 'evento_id', 'category_id' )
		            ->orderby( 'evento_estatus.id', 'desc' );
	}


	public function crear( $attributos = array() ) {
		return $this->create( $attributos );

	}
//=====================================================================
//funcion que devuelve el ultimo email

	static function getAdminEmail() {
		$email       = Email::all();

		$ultimoEmail = $email->last()->email;

		return $ultimoEmail;
	}

	static function email_notificar_contrato( $data ) {
		if ( empty( $data ) ) {
			return false;
		}


		$data['from'] = array(
			'email'  => 'no-responder@routeenjoy.com',
			'nombre' => 'Sistema Route Enjoy'
		);

		$data['subject'] = 'Route Enjoy (Registro de contrato)';
		$response        = Mail::queue(
			'notificarEvento',
			array( 'data' => $data ), function ( $message ) use ( $data ) {
				$message->to($data['usuario']['email'], $data['usuario']['first_name'] . " " . $data['usuario']['last_name'] );
				$message->from( $data['from']['email'], $data['from']['nombre'] );
				$message->subject( $data['subject'] );
			} );

		$response        = Mail::queue(
			'notificarEventoAdministrador',
			array( 'data' => $data ), function ( $message ) use ( $data ) {
				$message->to(self::getAdminEmail(), $data['usuario']['first_name'] . " " . $data['usuario']['last_name']);
				//$message->to( self::getAdminEmail(), $data['usuario']['first_name'] . " " . $data['usuario']['last_name'] );
				$message->from( $data['from']['email'], $data['from']['nombre'] );
				$message->subject( $data['subject'] );
			} );

		return $response;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 13/05/16 04:18 PM
	 */
	static function email_clave_aleatoria( $data ) {
		if ( empty( $data ) ) {
			return false;
		}

		$socio['from'] = array(
			'email'  => 'no-responder@routeenjoy.com',
			'nombre' => 'Sistema Route Enjoy'
		);

		$socio['subject'] = 'Route Enjoy (Clave de validación de evento' . $data['evento'] . ').';
		$response         = Mail::queue(
			'notificarClave',
			array( 'data' => $data ), function ( $message ) use ( $socio, $data ) {
				$message->to(  $data['cliente']['email'], $data['cliente']['first_name'] . " " . $data['cliente']['last_name'] );
				$message->from( $socio['from']['email'], $socio['from']['nombre'] );
				$message->subject( $socio['subject'] );
			} );

		return $response;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 16/05/16 10:44 PM
	 */
	static function validarEvento( $id ) {
		$evento  = self::find( $id );
		$estatus = Category::where( 'alt_value', 'en_proceso' )->get();

		$res = EventoEstatus::create( [ 'category_id' => $estatus[0]->id, 'evento_id' => $evento->id ] );

		return $res;
	}

	static function setEstatusDisfrutado( $id ) {
		$evento  = self::find( $id );
		$estatus = Category::where( 'alt_value', 'disfrutado' )->get();

		$res = EventoEstatus::create( [ 'category_id' => $estatus[0]->id, 'evento_id' => $evento->id ] );

		return $res;
	}

	public function rutas() {
		return $this->hasMany( 'App\Models\Rutas' );
	}

	public function email_cotizacion_contrato( $data = array() ) {
		//$datos = $this->get_cotizacion($evento);
		// var_dump($data); die;
		// var_dump($datos['evento']); die;
		$response = Mail::queue(
			'detallescontrato', array( 'data' => $data ), function ( $message ) use ( $data ) {
				//
				// $pdf = App::make('dompdf.wrapper');
				//$pdf->loadView($this->PdfCotizacionTemplate, $cotizacion);

				//$archivo = $pdf->setPaper('a4')->save('storage/DOC_' . $evento . '.pdf')->setWarnings(false);
				$data['from'] = array(
					'email'  => 'no-responder@routeenjoy.com',
					'nombre' => 'Sistema Route Enjoy'
				);
				//$data['attach'] = 'storage/DOC_' . $evento . '.pdf';

				$data['subject'] = 'Route Enjoy (Detalles de contrato)';
				//
				$message->to($data['usuario']['email'], $data['usuario']['first_name'] );
				$message->from( $data['from']['email'], $data['from']['nombre'] );
				$message->subject( 'Route Enjoy: Detalles de Su compra' );
				// $message->attach('storage/DOC_54BU.pdf', ['DOC_' . $evento . '.pdf']);
			} );

		return $data;
	}

	public function get_cotizacion( $evento_id ) {
		$cotizacion           = array();
		$evento               = self::find( $evento_id );
		$cliente              = $evento->persona()->get()->toArray();
		$rutas                = $evento->rutas()->get()->toArray();
		$cotizacion['evento'] = [
			'id'           => $evento->id,
			'cliente_id'   => $evento->cliente_id,
			'fecha_inicio' => $evento->fecha_inicio,
			'fecha_fin'    => $evento->fecha_fin,
			'iva'          => $evento->iva,
			'montoiva'     => $evento->montoiva,
			'total'        => $evento->total,
			'totaliva'     => $evento->totaliva,
			'rutas'        => $rutas[0],
			'cliente'      => $cliente,
		];
		$productos            = $evento->productosContratados()->get()->toArray();
		//var_dump($productos); die;
		foreach ( $productos as $key => $producto ) {
			$cotizacion['productos'][ $key ] =
				[
					'id'                     => $producto['id'],
					'descripcion'            => $producto['descripcion'],
					'producto_id'            => $producto['producto_id'],
					'costo_individual'       => $producto['costo_individual'],
					'servicio_contratado_id' => $producto['servicio_contratado_id'],
					'nombre'                 => $producto['nombre'],
					'image_path'             => $producto['image_path'],
					'cantidad'               => $producto['pivot']['cantidad'],
					'total'                  => $producto['pivot']['cantidad'] * $producto['costo_individual'],
				];

		}

		return $cotizacion;
	}

	/**
	 * @updated Amalio Velásquez
	 * @version 13/05/16 04:16 PM
	 */
	public function crearEvento( $request, $loggedUser ) {
		$DatosEvento = array();
		$DatosEvento = \DB::transaction( function () use ( $request, $loggedUser ) {
			try {
				if ( $loggedUser )://Si existe una sesion activa
					//capturar los datos del usuario y el id de persona

					$persona     = $loggedUser;
					$persona->id = $persona->persona_id;
				 else:
					$personaData         = [
						'nombre'          => $request->input( 'first_name' ),
						'apellido'        => $request->input( 'last_name' ),
						'ci'              => $request->input( 'cedula' ),
						'email'           => $request->input( 'email' ),
						'telefono'        => $request->input( 'phone' ),
						'tipo_persona_id' => Category::where( '_table', '=', 'tipo_persona' )
						                             ->where( '_label', '=', 'cliente' )
						                             ->pluck( 'id' ),
					];
					$personaData         = UserModel::encript_decrypt_datos( $personaData, 'encrypt', [
						'nombre',
						'apellido',
						'ci',
						'email',
						'telefono'
					] );
					$persona             = Persona::create( $personaData );
					$request->persona_id = $persona->id;
					$user                = UserModel::create_usuario( $request, true, true, true );
					// dd($user);
					if ( ! $user['response']['success'] ) {
						throw new Exception( $user['response']['messages'] );
					}
				endif;
				//dd((string) $request->input('fecha_desde'));
				$clave_aleatoria = str_random( 5 );

				$eventoData = [//DateTime::createFromFormat('d/m/Y H:i:s', $eventoData['fecha_inicio']);
					'cliente_id'      => $persona->id,
					'fecha_inicio'    => DateTime::createFromFormat( 'Y/m/d H:i:s', (string) $request->input( 'fecha_desde' ) ),
					'fecha_fin'       => DateTime::createFromFormat( 'Y/m/d H:i:s', (string) $request->input( 'fecha_hasta' ) ),
					'iva'             => $request->input( 'iva' ),
					'montoiva'        => $request->input( 'montoiva' ),
					'total'           => $request->input( 'total' ),
					'totaliva'        => $request->input( 'totalIva' ),
					'clave_aleatoria' => $clave_aleatoria,
					'alias'           => $request->input( 'alias' )
				];
				$evento     = self::create( $eventoData );

				$productosData = $request->input( 'products' );
				$contratado    = new ProductoContratado();
				foreach ( $productosData as $key => $producto ) {
					# code...
					if ( $producto['cantidad'] > 0 ):
						$productos[] = $contratado->findOrCreateProductoContratado( $producto );
					endif;

				}
				foreach ( $productos as $key => $producto ) {
					$evento->productosContratados()->attach( $producto );
				}
				$eventoData['rutas']           = [
					'punto_inicio' => $request->input( 'origin' ),
					'punto_fin'    => $request->input( 'destination' ),
					'free_route'   => $request->input( 'free_route' ),
					'arreglo'      => $request->input( 'ruta' ),
				];
				$rutas                         = Rutas::create(
					[
						'evento_id'    => $evento->id,
						'punto_inicio' => $eventoData['rutas']['punto_inicio'],
						'punto_fin'    => $eventoData['rutas']['punto_fin'],
						'ruta'         => json_encode( $eventoData['rutas']['arreglo'] ),
						'libre'        => $eventoData['rutas']['free_route'],
					]
				);
				$data['usuario']['email']      = $request->input( 'email' );
				$data['usuario']['first_name'] = $request->input( 'first_name' );
				$data['usuario']['last_name']  = $request->input( 'last_name' );
				//$fecha_inicio = DateTime::createFromFormat('d/m/Y H:i:s', $eventoData['fecha_inicio']);
				$fecha_inicio = $eventoData['fecha_inicio'];
				//$fecha_fin = DateTime::createFromFormat('d/m/Y H:i:s', $eventoData['fecha_fin']);
				$fecha_fin                  = $eventoData['fecha_fin'];
				$eventoData['fecha_fin']    = $fecha_fin->format( 'd-m-Y h:m a' );
				$eventoData['fecha_inicio'] = $fecha_inicio->format( 'd-m-Y h:m a' );
				$data['persona']            = [
					'nombre'   => $request->input( 'first_name' ),
					'apellido' => $request->input( 'last_name' ),
					'ci'       => $request->input( 'cedula' ),
					'email'    => $request->input( 'email' ),
					'telefono' => $request->input( 'phone' ),
				];
				$evento_insert              = self::find( $evento->id )->toArray();
				$data['evento']             = $eventoData;
				$data['evento']['codigo']   = $evento_insert['codigo'];
				$data['productos']          = $evento->productosContratados()->get()->toArray();
				Evento::email_notificar_contrato($data);
				$evento->email_cotizacion_contrato($data);
				$outPut = [
					//'user' => $user,
					'success'         => true,
					'persona'         => $persona->toArray(),
					'evento'          => $evento->toArray(),
					'evento_producto' => $evento->productosContratados()->get()->toArray(),
					'todos'           => $evento->get_cotizacion( $evento->id ),
				];

				return $outPut;
			} catch ( Exception $e ) {
				return array( 'success' => false, 'messages' => $e->getMessage() );
			}
		} );

		return $DatosEvento;
	}

	public function pagos() {
		return $this->belongsToMany( 'App\Models\Pagos', 'public.evento_pago', 'pago_id', 'evento_id' );
	}

	static function asignarRecursos( $evento, $recursos = array() ) {
		$evento = self::find( $evento );
		foreach ( $recursos as $k => $v ) {
			$recurso  = Recursos::find( $k );
			$pro_cont = ProductoContratado::all()->where( 'producto_id', $recurso->producto_id )->last();

			$recursos[ $k ]['producto_contratado_id'] = $pro_cont->id;

		}

		$evento->recursos()->sync( $recursos );


	}

	public function getRecursosDisponibles( $eventoID ) {
		$evento    = self::find( $eventoID );
		$productos = $evento->productosContratados()->with( 'producto' )
		                    ->whereDoesntHave( 'producto.recurso.evento', function ( $q ) {
			                    return $q;
		                    } )
		                    ->get()
		                    ->toArray();
		$out       = array();
		//$fecha_inicio = Carbon::createFromFormat('Y-m-d H:i:s', "2015-12-23 10:15:43");
		$fecha_inicio = Carbon::createFromFormat( 'Y-m-d H:i:s', $evento->fecha_inicio );
		//$fecha_fin = Carbon::createFromFormat('Y-m-d H:i:s', "2015-12-30 10:15:43");
		$fecha_fin       = Carbon::createFromFormat( 'Y-m-d H:i:s', $evento->fecha_fin );
		$rec             = Recursos::with( 'evento', 'producto.GananciaCompartida' )
		                           ->where( function ( $q ) use ( $productos ) {
			                           $q->where( 'id', '>', 0 );
			                           foreach ( $productos as $k => $v ) {
				                           $q->orWhere( 'producto_id', $v['producto']['id'] );
			                           }

			                           return $q;
		                           } )
		                           ->whereHas( 'producto.GananciaCompartida', function ( $q ) {
			                           return $q;
		                           } )->get()->toArray();
		$out['recursos'] = [ ];
		foreach ( $rec as $key1 => $recurso ) :
			$cant_eventos = 0;
			if ( count( $recurso['evento'] ) > 0 ):
				foreach ( $recurso['evento'] as $key2 => $eventos ) :
					$rec_fecha_inicio = Carbon::createFromFormat( 'Y-m-d H:i:s', $eventos['fecha_inicio'] )->between( $fecha_inicio, $fecha_fin );
					$rec_fecha_fin    = Carbon::createFromFormat( 'Y-m-d H:i:s', $eventos['fecha_fin'] )->between( $fecha_inicio, $fecha_fin );

					if ( $rec_fecha_inicio == true || $rec_fecha_fin == true ):
						$cant_eventos ++;
					endif;
				endforeach;
				if ( $cant_eventos == 0 ):
					unset( $recurso['evento'] );
					if ( count( $recurso['producto']['ganancia_compartida'] ) > 0 ) :
						foreach ( $recurso['producto']['ganancia_compartida'] as $key3 => $ganancia ) :
							if ( $ganancia['id'] == $recurso['socio_id'] ) :
								$recurso['ganancia_compartida'] = $ganancia['pivot'];
								unset( $recurso['producto'] );
							else :
								//$recurso['ganancia_compartida'] = [];
								//$recurso = [];
							endif;

						endforeach;
					else :
						//$recurso['ganancia_compartida'] = [];
						//unset($recurso['producto']);
						//$recurso = [];
					endif;
					$out['recursos'][] = $recurso;
				endif;
			endif;

		endforeach;
		$cantRecContrat = 0;
		foreach ( $productos as $k => $v ) {
			$cantRecContrat = $cantRecContrat + $v['pivot']['cantidad'];
		}

		$out['parcial'] = ( count( $cantRecContrat ) == count( $out['recursos'] ) ) ? false : true;

		return $out;
	}

	public function getEventosPorAsignar() {
		$eventos = Evento::all();
		foreach ( $eventos as $key => $evento ) {
			$cantRecContrat = 0;
			$productos      = $evento->productosContratados()->with( 'producto' )
				/*->whereDoesntHave('producto.recurso.evento', function ($q) {
					return $q;
				})*/
				                     ->get()
				                     ->toArray();
			foreach ( $productos as $k => $v ) {
				$cantRecContrat = $cantRecContrat + $v['pivot']['cantidad'];
			}
			$recursos = $evento->recursos()->get()->toArray();
			$parcial  = ( count( $cantRecContrat ) == count( $recursos ) ) ? false : true;

			if ( $parcial ) {
				$this->asignarRecursosAuto( $evento->id );
			}
		}

	}

	public function arrangeDataToSync( array $datos ) {
		$arreglo = array();
		foreach ( $datos as $key => $value ) {
			$arreglo[ $value['id'] ] = [ 'porcentaje_socio' => $value['ganancia_compartida']['porcentaje_socio'] ];
		}

		return $arreglo;

	}

	public function asignarRecursosAuto( $eventoID ) {
		$arregloRecursos = $this->getRecursosDisponibles( $eventoID );
		$recursos        = $this->arrangeDataToSync( $arregloRecursos['recursos'] );
		if ( count( $recursos ) > 0 ):
			self::asignarRecursos( $eventoID, $recursos );
		else:
			//notificar al administrador
		endif;

		return $recursos;
	}

	static function getRecursosByEvento( $evento_id ) {
		$evento   = self::find( $evento_id );
		$recursos = $evento->recursos()->get()->toArray();
		$out      = [ ];
		foreach ( $recursos as $key => $recurso ) {
			$out[ $recurso['producto_id'] ][] = $recurso;
		}

		return $out;
	}

	static function getEventoCliente( $cliente_id ) {
		$eventos             = self::with( 'estatus_evento' )->where( 'cliente_id', $cliente_id )->orderBy( 'id', 'desc' )->get();
		$eventos_espera_pago = array();
		foreach ( $eventos as $evento ) {
			if ( $evento->estatus_evento->last()->alt_value === "en_espera_de_pago" ) {
				$eventos_espera_pago[] = $evento;
			}
		}

		return $eventos_espera_pago;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 02/05/16 10:50 PM
	 */

	public function getEventosSocio( $id ) {
		$recursos = Recursos::where( 'socio_id', $id )->get();

		$evento_recurso = array();
		foreach ( $recursos as $recurso ) {
			$result = EventoRecurso::where( 'recurso_id', $recurso['id'] )->get()->toArray();

			foreach ( $result as $recurso ) {
				if (!in_array( $recurso['evento_id'], $evento_recurso )) {
					$evento_recurso[] = $recurso['evento_id'];
				}
			}
		}

		$eventos = Evento::whereIn( 'id', $evento_recurso )->get();
		return $eventos;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 02/05/16 10:50 PM
	 */
	public function getDetallesEventoSocio( $id ) {
		$evento = Evento::find( $id );
		$evento->persona;
		$evento->rutas;
		$evento->productosContratados;

		return $evento;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 03/05/16
	 */
	public function getRecursosSocio( $id ) {
		$recursos = Recursos::where( 'socio_id', $id )->get();

		foreach ( $recursos as $recurso ) {
			if ( $recurso->persona_recurso_id == true ) {
				$recurso->persona_recurso_id = 'Persona';
			} else {
				$recurso->persona_recurso_id = 'Vehiculo';
			}
		}

		return $recursos;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 03/05/16
	 */
	public function getDetallesRecursoSocio( $id ) {
		$recurso  = new Recursos();
		$recurso2 = $recurso->find( $id )->toArray();
		if ( $recurso2['persona_recurso_id'] == true ) {
			$recurso2['persona_recurso_id'] = 'Persona';
		} else {
			$recurso2['persona_recurso_id'] = 'Vehiculo';
		}

		$productos = $recurso->find( $id )->producto()->get()->toArray();
		$vehiculo  = $recurso->find( $id )->vehiculoRecurso()->get()->toArray();
		$persona   = $recurso->find( $id )->personaRecurso()->get()->toArray();
		if ( $vehiculo ) {
			$detales_tipo_recurso = $vehiculo;
		} else if ( $persona ) {
			$detales_tipo_recurso = $persona;
		}
		$result = [ $recurso2, $productos, $detales_tipo_recurso ];

		return $result;
	}


	/* public function getFechaInicioAttribute($value)
	{
		$fecha_fin = DateTime::createFromFormat('Y-m-d H:i:s', $value);
		return $fecha_fin->format('d/m/Y h:i:s a');
	}
	public function getFechaFinAttribute($value)
	{
		$fecha_fin = DateTime::createFromFormat('Y-m-d H:i:s', $value);
		return $fecha_fin->format('d/m/Y h:i:s a');
	}*/


	/**
	 * Recursos disponibles de acuerdo a estatus y producto
	 *
	 * @access  pivate
	 *
	 * @param array $params Arreglo de datos pertinentes a la consulta
	 *
	 * @return array
	 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
	 * @version V 1.0 08/12/15 04:12 PM
	 */
	public function recursos_disponibles( $params = array() ) {
		//Se obtienen los recursos disponibles de acuerdo a la fecha y el producto en un evento
		$recursos1 = Recursos::whereHas( 'evento_recurso.eventos', function ( $q ) use ( $params ) {
			return $q->whereNotBetween(
				'fecha_inicio', [ $params['fecha_inicio'], $params['fecha_fin'] ]
			);

		} )->whereHas( 'evento_recurso', function ( $q ) use ( $params ) {
			return $q->where( 'evento_id', '<>', $params['evento_id'] );

		} )->where( 'producto_id', '=', $params['producto_id'] )
		                     ->where( 'activo', '=', true )->get()->toArray();

		//Se obtienen los recursos que nunca han sigo asignados a un evento
		$recursos2 = Recursos::whereDoesntHave( 'evento_recurso', function ( $q ) use ( $params ) {
			return $q->where( 'evento_id', '<>', $params['evento_id'] );

		} )->where( 'producto_id', '=', $params['producto_id'] )
		                     ->where( 'activo', '=', true )->get()->toArray();

		return array_merge( $recursos1, $recursos2 );
	}


	/**
	 * Eventos pendientes para aplicar asignacion de recursos
	 *
	 * @access  public
	 * @return mixed
	 * @throws Exception
	 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
	 * @version V 1.0 08/12/15 04:12 PM
	 */
	public function eventos_pendientes() {
		$fecha_actual=date('Y-m-d H:i:s',time());

		$eventos = self::where('fecha_inicio', '>', $fecha_actual)->
		whereHas( 'ultimo_estatus_evento', function ( $q ) {
			$q->where( '_label', '=', 'Validado' );
		} )->with( [
			'ultimo_estatus_evento',
			'evento_producto.producto_contratado',
			//'evento_producto',
			'persona',
			'recursos'
		] )->whereDoesntHave( 'recursos' )->get();

		if ( $eventos->count() > 0 ) {
			return $eventos;
		}

		//para devolver eventos con aignacion incompleta
		$eventos = self::where('fecha_inicio', '>', $fecha_actual)->
		whereHas( 'ultimo_estatus_evento', function ( $q ) {
			$q->where( '_label', '=', 'Validado' );
		} )->with( [
			'ultimo_estatus_evento',
			'evento_producto.producto_contratado',
			'recursos'
		] )->Has( 'recursos' )->get();

		if ( $eventos->count() < 1 ) {
			return false;
		}

		foreach($eventos as $evento){
			$cantidad=0;
			$id_eventos=[];
			foreach($evento->evento_producto as $eve_pro){
				$cantidad+=$eve_pro->cantidad;
			}

			if($cantidad > $evento->recursos->count()){
				array_push($id_eventos, $evento->id);
			}
		}

		if ( count($id_eventos) < 1 ) {
			return false;
		}

		$eventos = self::where('fecha_inicio', '>', $fecha_actual)->
		whereIn('id', $id_eventos)->with( [
			'ultimo_estatus_evento',
			'evento_producto.producto_contratado',
			'persona',
			'recursos'
		] )->get();

		return $eventos;
	}

	/**
	 * Asignación Automatica de recursos tomando en cuenta los socios.
	 *
	 * @param Command $command
	 * @param bool    $considerar_cantidad
	 *
	 * @return bool
	 * @updated Amalio Velásquez
	 * @version 13/05/2016 04:25 PM
	 */
	public function asignar_recurso_auto( Command $command, $considerar_cantidad = true ) {
		$notificaciones = [
			'failure' => [ ],
			'success' => [ ]
		];

		$eventos_pendientes = $this->eventos_pendientes();
		if ( empty( $eventos_pendientes ) ) {
			return false;
		}

		try {
			foreach ( $eventos_pendientes as $evento ) {
				$data_email = array();

				$evento_no_asignado = false;
				$recursos_asigandos = [ ];

				$params = [
					'inicio'        => $evento->fecha_inicio,
					'fin'           => $evento->fecha_fin,
					'socios'        => [ ], // Contiene los IDs de los socios que aportan recursos
					'seleccionados' => [ ]
				];


				foreach ( $evento->evento_producto as $evento_producto ) {
					$params['producto_contratado_id'] = $evento_producto->producto_contratado_id;

					$recursos       = new Recursos();
					$recursos       = $recursos->disponibles( $params );
					$total_recursos = count( $recursos );

					// Valido si la cantidad solicitada de recursos es mayor a lo existente en stock.
					if ( $considerar_cantidad && $evento_producto->cantidad > $total_recursos ) {
						$evento_no_asignado = true;
					}

					$isVehiculo = $evento_producto->producto_contratado->isVehiculo();

					$len = ( $considerar_cantidad || $evento_producto->cantidad <= $total_recursos ) ? $evento_producto->cantidad : $total_recursos;
					$len -= 1;

					// Le damos prioridad a los recursos no asigandos previamente,
					//recorriendo el arreglo de atras hacia delante.
					//dd($len, $evento_producto->cantidad, $total_recursos);
					for ( $i = $len; $i >= 0; $i -- ) {
						$params['seleccionados'] = $recursos_asigandos;

						foreach($evento->recursos as $recurso){
							foreach($recursos as $key => $recursito){
								if($recurso->id == $recursito['id']){
									unset($recursos[$key]);
								}
							}
						}

						$resources               = $this->seleccionarRecurso( $recursos, $params, $isVehiculo );
						
						if ( empty( $resources ) ) {
							$evento_no_asignado = $considerar_cantidad;
							break;
						}

						foreach ( $resources as $resource ) {
							$flag=false;
							$eve=Evento::where('id',$evento->id)->with('evento_recurso')->get();

							if(!$eve[0]->evento_recurso->isEmpty()){
								foreach($eve[0]->evento_recurso as $recurso){
									if($recurso->recurso_id == $resource['recurso']['id']){
										$flag=true;
									}
								}
							}

							if(!$flag){
								// Lleno el valor del socio.
								if ( ! in_array( $resource['recurso']['socio_id'], $params['socios'] ) ) {
									array_push( $params['socios'], $resource['recurso']['socio_id'] );
								}

								// Asigno en memoria el recurso.
								array_push( $recursos_asigandos, $resource['recurso']['id'] );
							}
						}
					}

					if ( $evento_no_asignado === true ) {
						break;
					}
				}

				// El evento no fue asignado por falta de recursos.
//                if ($evento_no_asignado) {
//                    //$command->info($considerar_cantidad . ':..catidad || Evento no asignado por falta de recursos :' . $evento->codigo);
//                    //array_push($notificaciones['failure'], $evento);
//                    continue;
//                }

				$recursos2 = array();
				//dd($recursos_asigandos);
				if ( $recursos_asigandos ) {
					foreach ( $recursos_asigandos as $k => $v ) {
						$recursos2[ $v ] = [ ];
					}

					foreach ( $recursos2 as $k => $v ) {
						$recurso    = Recursos::find( $k );
						$pro_cont   = ProductoContratado::all()->where( 'producto_id', $recurso->producto_id )->last();
						$porcentaje = GananciaCompartida::all()->where( 'producto_id', $recurso->producto_id )->last();

						if($porcentaje->porcentaje_socio != null && $porcentaje->porcentaje_socio >= 1){
							$recursos2[ $k ]['producto_contratado_id'] = $pro_cont->id;
							$recursos2[ $k ]['porcentaje_socio']       = $porcentaje->porcentaje_socio;
						}
					}

					foreach ( $recursos2 as $k => $v ) {
						if(!isset($recursos2[ $k ]['porcentaje_socio'])){
							unset($recursos2[$k]);
						}
					}
					
					if(count($recursos2) > 0){
						$evento->recursos()->attach( $recursos2 );
						array_push( $notificaciones['success'], $evento->id );
					}
				}
			}

			/*if (empty($notificaciones['success']) && empty($notificaciones['failure'])) {
				// No hago nada xq no hay nada que notificar.
				return false;
			}*/

//			 Notifico la primera pasada que contiene todos loes eventos con asignación total
            if ($considerar_cantidad && !empty($notificaciones['success'])) {
                $this->enviarNotificacion($notificaciones['success']);
            } else if (!$considerar_cantidad && !empty($notificaciones['success'])) {
                $this->enviarNotificacion($notificaciones['success'], true);
            }

			//$this->asignar_recurso_auto(false);
			//$this->asignar_recurso_auto($command, false);
		} catch ( Exception $e ) {
			$command->error( $e->getMessage() );
			$command->error( $e->getLine() );
			$command->error( $e->getFile() );
		}

		if ( $considerar_cantidad ) {
			$this->asignar_recurso_auto( $command, false );
		}

		// @todo Configurar notificación de correo.
		//dd($notificaciones);
	}

	/**
	 * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
	 * @version V 1.0 08/12/15 04:12 PM
	 * @updated Amalio Velásquez
	 * @version 11/05/16 10:50 PM
	 */
	public function ganancia_compartida( $id_evento = null, $id_socio = null ) {
		$out    = array();
		$evento = self::with( [
			'recursos.persona',
		] )->find( $id_evento );

		if ( $evento ) {
			$evento = $evento->toArray();

			$socios = array();
			if ( $id_socio == ! null ) {
				$socios[] = $id_socio;
			} else {
				foreach ( $evento['recursos'] as $v ) {
					if ( ! in_array( $v['persona']['id'], $socios ) ) {
						$socios[] = $v['persona']['id'];
					}
				}
			}

			foreach ( $socios as $socio ) {
				$suma = 0;

				foreach ( $evento['recursos'] as $k => $v ) {
					$e = EventoRecurso::where( 'evento_id', $id_evento )->where( 'recurso_id', $v['pivot']['recurso_id'] )->get();
					$p = ProductoContratado::find( $e[0]->producto_contratado_id );
					if ( $p != null ) {
						$v['costo_individual'] = $p->costo_individual;
					}

					if ( ! isset( $out[ $v['socio_id'] ] ) ) {
						if ( $id_socio == ! null && $id_socio == $v['socio_id'] ) {
							$out[ $v['socio_id'] ] = $v['persona'];
						} else if ( $id_socio == null && $socio == $v['socio_id'] ) {
							$out[ $v['socio_id'] ] = $v['persona'];
							//$out[$v['socio_id']]['total_recursos'] = 0.00;
						}
					}

					if ( isset( $out[ $v['socio_id'] ] ) && $socio == $v['socio_id'] ) {
						$suma += ( $v['costo_individual'] * $v['pivot']['porcentaje_socio'] ) / 100;
						$out[ $v['socio_id'] ]['subtotal_socio']   = number_format( $suma, 2, ',', '.' );
						$out[ $v['socio_id'] ] ['iva']             = $evento['iva'];
						$out[ $v['socio_id'] ] ['monto_iva']       = $out[ $v['socio_id'] ]['subtotal_socio'] * $evento['iva'] / 100;
						$out[ $v['socio_id'] ] ['total_socio_iva'] = number_format( $suma + $out[ $v['socio_id'] ] ['monto_iva'], 2, ',', '.' );
					}

					if ( $socio == $v['socio_id'] ) {
						$out[ $v['socio_id'] ]['recursos'][ $v['id'] ]                     = $v;
						$out[ $v['socio_id'] ]['recursos'][ $v['id'] ]['costo_individual'] = number_format( $v['costo_individual'], 2, ',', '.' );;
						$out[ $v['socio_id'] ]['recursos'][ $v['id'] ]['monto_socio'] = number_format( $v['costo_individual'] * $v['pivot']['porcentaje_socio'] / 100, 2, ',', '.' );;
					}
				}
			}

			return $out;
		}
		$eve = $evento;

		return $eve;
	}

	/**
	 * Selecciona los recursos a ser empleados en el evento, en caso de ser un recurso de tipo vehiculo
	 * selecciona su conductor.
	 * En caso de no encontrar un conductor disponible retorna false.
	 *
	 * @param array $recursos
	 * @param array $params
	 * @param bool  $isVehiculo
	 *
	 * @return array|bool
	 * @author Jose Rodriguez
	 */
	private function seleccionarRecurso( $recursos, $params, $isVehiculo = false ) {
		$selected = [ ];
		// Ya tengo socio previamente seleccionado.
		if ( ! empty( $params ) ) {
			foreach ( $recursos as $key => $recurso ) {
				if ( in_array( $recurso['id'], $params['seleccionados'] ) ) {
					continue;
				}

				if ( in_array( $recurso['socio_id'], $params['socios'] ) ) {
					array_push( $selected, [ 'key' => $key, 'recurso' => $recurso ] );
					break;
				}
			}
		}


		// Si no hay seleccionado, quiere decir que no hay recurso disponible
		// perteneciente a los socios previamente seleccionados.
		if ( empty( $selected ) ) {
			array_push( $selected, [ 'recurso' => current( $recursos ), 'key' => key( $recursos ) ] );
		}

		if ( $isVehiculo && ! empty( $selected[0] ) ) {
			$recurso = Recursos::find( $selected[0]['recurso']['id'] );
			$recurso = $recurso->getChoferRelacionado( $params );

			if ( empty( $recurso ) ) {
				return false;
			}

			array_push( $selected, [ 'recurso' => $recurso ] );
		}

		return $selected;
	}

	/**
	 * Envia la notificación de correo a los involucrados.
	 *
	 * @param      $eventos
	 * @param bool $incidencias
	 */
	private function enviarNotificacion( $eventos, $incidencias = false ) {
		$data                     = $this->generarExcel( $eventos );
		$email['APP_DESCRIPTION'] = getenv( 'APP_DESCRIPTION' );
		$email['MAIL_USERNAME']   = getenv( 'MAIL_USERNAME' );
		$email['APP_NAME']        = getenv( 'APP_NAME' );

		foreach ( $data as $key => $row ) {
			$email['subject'] = 'Notificación de Modulo Evento';
			if ( $key == 'admin' ) {
				$user_object  = new UserModel();
				$data_encrypt = $user_object->where( 'active', '=', 1 )->whereHas( 'group', function ( $q ) {
					return $q->where( 'group_id', '=', 1 );
				} )->get()->toArray();

				/*$data_encrypt = $user_object->encript_decrypt_datos(
					$data_encrypt, //Datos a procesar
					'decrypt',//Modo que aplicara
					[ 'first_name', 'last_name' ]//Claves que se le aplicara encriptacion
				);*/

				$email['email'] = [ ];
				foreach ( $data_encrypt as $admin ) {
					array_push( $email['email'], $admin['email'] );
				}

				if ( $incidencias ) {
					$email['subject'] .= ' - con incidencias';
				}
			} else {
				$email['email']  = $row[0]['socio_correo'];
				$email['nombre'] = $row[0]['socio_nombre'];
			}

			$view = 'emails.asignacion_automatica';

			Mail::queue(
				$view,
				[ 'data' => $data ],
				function ( $message ) use ( $email, $key ) {
					if ( is_array( $email['email'] ) ) {
						$message->to( $email['email'] );
					} else {
						$message->to( $email['email'], $email['nombre'] );
					}

					$message->from( $email['MAIL_USERNAME'], $email['APP_NAME'] );
					$bcc = [ $this->getAdminEmail() ];
					$message->bcc( $bcc );
					$message->subject( $email['subject'] );
					$message->attach( storage_path( 'exports' ) . '/socio_' . $key . '.xls' );
				}
			);
		}

		if ( $incidencias ) {
			$data             = $this->generarExcelIncidencias( $eventos );
			$email['subject'] = 'Notificación de Modulo Evento - Relación de Eventos y Recursos Faltantes';

			$user_object  = new UserModel();
			$data_encrypt = $user_object->where( 'active', '=', 1 )->whereHas( 'group', function ( $q ) {
				return $q->where( 'group_id', '=', 1 );
			} )->get()->toArray();

			/*$data_encrypt = $user_object->encript_decrypt_datos(
				$data_encrypt, //Datos a procesar
				'decrypt',//Modo que aplicara
				[ 'first_name', 'last_name' ]//Claves que se le aplicara encriptacion
			);*/

			$email['email'] = [ ];
			foreach ( $data_encrypt as $admin ) {
				array_push( $email['email'], $admin['email'] );
			}

			$view = 'emails.asignacion_automatica_incidencias';

			Mail::queue(
				$view,
				[ 'data' => $data ],
				function ( $message ) use ( $email ) {
					$message->to( $email['email'] );
					$message->from( $email['MAIL_USERNAME'], $email['APP_NAME'] );
					$bcc = [ $this->getAdminEmail() ];
					$message->bcc( $bcc );
					$message->subject( $email['subject'] );
					$message->attach( storage_path( 'exports' ) . '/incidencias.xls' );
				}
			);

		}

	}

	/**
	 * Genera archivos de excel en función del la data recibida.
	 *
	 * @param $eventos
	 *
	 * @return array
	 * @author Jose Rodriguez
	 */
	private function generarExcel( $eventos ) {
		$data = [ 'admin' => [ ] ];
		$item = 0;

		foreach ( $eventos as $evento ) {
			$evento   = Evento::find( $evento );
			$recursos = $evento
				->recursos()
				->with( [ 'persona', 'personaRecurso', 'vehiculoRecurso' ] )
				->where( 'activo', true )
				->get();

			foreach ( $recursos as $key => $recurso ) {
				if ( ! array_key_exists( $recurso->socio_id, $data ) ) {
					$data[ $recurso->socio_id ] = [ ];
				}

				$row = [
					'item'           => ++ $item,
					'evento_codigo'  => $evento->codigo,
					'clave_aleatoria' => $evento['clave_aleatoria'],
					'evento_inicio'  => $evento->fecha_inicio,
					'evento_fin'     => $evento->fecha_fin,
					'socio_nombre'   => "{$recurso->persona->apellido}, {$recurso->persona->nombre}",
					'socio_rif'      => $recurso->persona->rif,
					'socio_telefono' => $recurso->persona->telefono,
					'socio_correo'   => $recurso->persona->email,
					'recurso_codigo' => $recurso->codigo,
				];

				$descripcion = [ ];
				if ( ! empty( $recurso->personaRecurso ) ) {
					$descripcion = [
						"Nombre.: {$recurso->personaRecurso->apellido}, {$recurso->personaRecurso->nombre}",
						"Teléfono.: {$recurso->personaRecurso->telefono}",
						"Correo.: {$recurso->personaRecurso->email}",
						"Descripción.: {$recurso->descripcion}",
					];
				} else {
					$descripcion = [
						"Descripción.: {$recurso->descripcion}",
						"Modelo.: {$recurso->vehiculoRecurso->modelo}",
						"Placa.: {$recurso->vehiculoRecurso->nro_placa}",
						"Color.: {$recurso->vehiculoRecurso->color}",
						"Carroceria.: {$recurso->vehiculoRecurso->carroceria}",
					];
				}

				$row['recurso_descripcion'] = implode( ', ', $descripcion );
				array_push( $data['admin'], $row );
				array_push( $data[ $recurso->socio_id ], $row );
			}
		}

		foreach ( $data as $key => $rows ) {
			Excel::create( "socio_{$key}", function ( $excel ) use ( $rows ) {
				$excel->setTitle( 'Relación Recursos asignados a Eventos' );
				$excel->setCreator( getenv( 'APP_NAME' ) )->setCompany( getenv( 'APP_NAME' ) );
				$excel->setDescription( 'Relación Recursos asignados a Eventos' );

				$excel->sheet( 'Sheetname', function ( $sheet ) use ( $rows ) {
					//$sheet->setWidth('A', 5);
					$headers = [
						'# Item',
						'Evento',
						'Clave de validación',
						'Fecha de Inicio',
						'Fecha Fin',
						'Colaborador',
						'Telefono',
						'Correo',
						'RIF',
						'Recurso Codigo',
						'Descripción'
					];

					$final= [ "Archivo generado automaticamente por " . getenv( 'APP_NAME' ) ];
					array_unshift( $rows, $headers );
					array_push( $rows, $final );

					$sheet->fromArray( $rows );
//					$sheet->protect( '23eqws2' );
				} );
			} )->store( 'xls' );
		}

		return $data;
	}

	/**
	 * Genera el excel de eventos con incidencias
	 *
	 * @param $eventos
	 */
	private function generarExcelIncidencias( $eventos ) {
		$eventos = self
			::with( [
				'productosContratados.producto.tipo_producto',
				'recursos.personaRecurso',
				'recursos.vehiculoRecurso',
			] )
			->whereIn( 'id', $eventos )
			->get();

		$rows = [ ];
		$item = 0;

		foreach ( $eventos as $key => $evento ) {
			$evento = $evento->toArray();

			foreach ( $evento['productos_contratados'] as $contratado ) {
				$row = [
					'item'              => ++ $item,
					//'evento_id' => $evento['id'],
					'evento_codigo'     => $evento['codigo'],
					'evento_inicio'     => $evento['fecha_inicio'],
					'evento_fin'        => $evento['fecha_fin'],
					'producto_nombre'   => $contratado['producto']['nombre'],
					'producto_tipo'     => $contratado['producto']['tipo_producto']['_label'],
					'producto_cantidad' => $contratado['pivot']['cantidad'],
				];

				$contador      = 0;
				$descripciones = [ ];
				foreach ( $evento['recursos'] as $recurso ) {
					if ( $contratado['producto_id'] == $recurso['producto_id'] ) {
						if ( ! empty( $recurso['persona_recurso'] ) ) {
							array_push( $descripciones, implode( ',', [
								"Nombre.: {$recurso['persona_recurso']['apellido']}, {$recurso['persona_recurso']['nombre']}",
								"Teléfono.: {$recurso['persona_recurso']['telefono']}",
								"Correo.: {$recurso['persona_recurso']['email']}",
								"Descripción.: {$recurso['descripcion']}"
							] ) );
						} else {
							array_push( $descripciones, implode( ',', [
								"Descripción.: {$recurso['descripcion']}",
								"Modelo.: {$recurso['vehiculo_recurso']['modelo']}",
								"Placa.: {$recurso['vehiculo_recurso']['nro_placa']}",
								"Color.: {$recurso['vehiculo_recurso']['color']}",
								"Carroceria.: {$recurso['vehiculo_recurso']['carroceria']}"
							] ) );
						}
						$contador ++;
					}
				}

				$row['recurso_cantidad']    = $contador;
				$row['recurso_descripcion'] = implode( ' // ', $descripciones );
				array_push( $rows, $row );
			}
		}

		Excel::create( "incidencias", function ( $excel ) use ( $rows ) {
			$excel->setTitle( 'Relación Recursos socilitados contra asignados por eventos' );
			$excel->setCreator( getenv( 'APP_NAME' ) )->setCompany( getenv( 'APP_NAME' ) );
			$excel->setDescription( 'Relación Recursos socilitados contra asignados por eventos' );

			$excel->sheet( 'Sheetname', function ( $sheet ) use ( $rows ) {
				//$sheet->setWidth('A', 5);
				$headers = [
					'# Item',
					'Evento',
					'Fecha de Inicio',
					'Fecha Fin',
					'Producto',
					'Tipo Producto',
					'Cantidad Solicitada',
					'Cantidad Asignada',
					'Recursos',
				];

				$final = [ "Archivo generado automaticamente por " . getenv( 'APP_NAME' ) ];
				array_unshift( $rows, $headers );
				array_push( $rows, $final );

				$sheet->fromArray( $rows );
				//$sheet->protect('23eqws2');
			} );
		} )->store( 'xls' );

		return $rows;
	}

	/**
	 * @author  Amalio Velásquez
	 * @version 24/05/16 12:07 PM
	 */
	public static function emailDesvioRuta( $data ) {
		$data = json_decode( $data );
		$link = "https://www.google.co.ve/maps/place/" . $data->lat . "," . $data->lng;

		$evento               = self::find( $data->evento->id );
		$remitente['from']    = array(
			'email'  => 'no-responder@route-enjoy.com',
			'nombre' => 'Sistema Route Enjoy'
		);
		$remitente['subject'] = 'Route Enjoy (Notificación de cambio de ruta en el evento ' . $evento->codigo . ').';

		$response = Mail::queue(
			'cambioRuta',
			array( 'evento' => $evento, 'link' => $link ), function ( $message ) use ( $remitente, $evento, $link ) {
				$message->to(self::getAdminEmail());
				$message->from( $remitente['from']['email'], $remitente['from']['nombre'] );
				$message->subject( $remitente['subject'] );
			} );

		return response()->json( [ 'succes' => 'true' ] );
	}
}
