<?php namespace App\Models;

use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Mail;
use App\Models\GroupModel;

class UserTokenModel extends Model
{
    protected $table = 'acl.users_token';

    /**
     * The primary key  used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Permite activar o desactivar
     * la insercion  o actualizacion en los campos
     * update, create que tiene por defecto laravel
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'usuario_id',
        'fechacrea',
        'fechacadu',
        'usado',
        'id',
        'validado',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /*
    * Relacion de los usuarios
    */
    public function user()
    {
        return $this->belongsTo('App\Models\UserModel','usuario_id');
    }

}


