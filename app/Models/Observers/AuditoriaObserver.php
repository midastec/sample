<?php
namespace App\Models\Observers;

use App\Models\Acl\Auditoria;

/**
 * Observer de los Models de Laravel para la auditoría
 * Created by PhpStorm.
 * User: jrodriguez
 * Date: 12/08/15
 * Time: 04:18 PM
 * @see http://laravel.com/docs/4.2/eloquent#model-events
 * @author Jose Rodriguez
 */
class AuditoriaObserver
{
    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function creating($model) {}

    /**
     * Evento de Laravel Models
     * @param $model
     */
    public function created($model)
    {
        $this->register($model);
    }

    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function saving($model){}

    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function saved($model) {}

    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function updating($model){}

    /**
     * Evento de Laravel Models
     * @param $model
     */
    public function updated($model)
    {
        $this->register($model);
    }

    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function deleting($model){}

    /**
     * Evento de Laravel Models
     * @param $model
     */
    public function deleted($model)
    {
        $this->register($model);
    }

    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function restoring($model){}

    /**
     * Evento de Laravel Models
     * @param $model
     */
//    public function restored($model){}

    /**
     * Realiza el llamado de actualización de la auditoría del sistema.
     * @param $model
     */
    private function register($model)
    {
//        dd($model->getTable());
        $auditoria = new Auditoria();
        $auditoria->actualizarAuditoria($model, debug_backtrace()[1]['function']);
    }
}