<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Acl\User;
use yajra\Datatables\Datatables;

class Contactos extends Model
{

    protected $table = 'contactos';

    public $timestamps = false;

    protected $fillable = [
        "nombre",
        "apellido",
        "email",
        "telefono",
        "parentezco",
        "persona_id",
        "usuario_id",
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
    ];

    /**
     * Relación con el modelo de Persona
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function persona ()
    {
        return $this->belongsTo ('App\Models\Persona');
    }

    static function dt_contactos ($idpersona)
    {
        $persona = Persona::find ($idpersona);
        if (!$persona) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $contactos = $persona->contactos()->with('persona');


        return Datatables::of ($contactos)->make (true);
    }

    static function index_contactos ($idpersona)
    {

        $persona = Persona::find ($idpersona);

        if (!$persona) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }

        return response ()->json (['status' => 'ok', 'data' => $persona->contactos()->get()], 200);

    }

    static function create_contacto($request, $idpersona, $user = null)
    {
        $persona = Persona::find($idpersona);
        if (!$persona) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $v = Validator::make($request->all(), [
            "nombre" => 'required',
            "apellido" => 'required',
            "telefono" => 'required',
            "parentezco" => 'required',
            "email" => 'required',
            "persona_id" => 'required',
        ]);
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }
        $row['usuario_id'] = $user->id;
        $nuevoContacto = self::firstOrNew(array('id' => $request->id));
	    $nuevoContacto->nombre = $request->nombre;
	    $nuevoContacto->apellido = $request->apellido;
	    $nuevoContacto->telefono = $request->telefono;
	    $nuevoContacto->parentezco = $request->parentezco;
	    $nuevoContacto->email = $request->email;
	    $nuevoContacto->persona_id = $request->persona_id['id'];
	    $nuevoContacto->save();
	    return $nuevoContacto;
    }

    static function update_contacto($request, $idpersona, $idcontacto)
    {
        $persona = Persona::find($idpersona);
        if (!$persona) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $contacto = $persona->contactos()->find($idcontacto);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $telefono = $request->input('telefono');
        $parentezco = $request->input('parentezco');
        $email = $request->input('email');
        $persona_id = $request->input('persona_id');

        $bandera = false;
        if ($nombre) {
            $contacto->nombre = $nombre;
            $bandera = true;
        }
        if ($apellido) {
            $contacto->apellido = $apellido;
            $bandera = true;
        }
        if ($telefono) {
            $contacto->telefono = $telefono;
            $bandera = true;
        }
        if ($parentezco) {
            $contacto->parentezco = $parentezco;
            $bandera = true;
        }
        if ($email) {
            $contacto->email = $email;
            $bandera = true;
        }
        if ($persona_id) {
            $contacto->persona_id = $persona_id;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $contacto->usuario_id = $user->findByName($request->header('username'), true)->id;
            $contacto->save();
            return response()->json(['status' => 'ok', 'data' => $contacto], 200, array('Location' => 'localhost/laravel/api/public/clientes/clientes/' . $persona->id . '/contactos/' . $contacto->id));
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato del contacto.'])], 304);
        }
    }

    static function show_contacto($idpersona, $idcontacto)
    {
        $persona = Persona::find($idpersona);
        if (!$persona) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $contacto = $persona->contactos()->with('persona')->find($idcontacto);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $contacto->toArray()], 200);
    }


}