<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{

    protected $table = 'vehiculo';

    public $timestamps = false;

    protected $fillable = [
        "serial_motor",
        "color",
        "serial",
        "nro_placa",
        "usuario_id",
        "modelo",
        "image_path",
        "carroceria"
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
}