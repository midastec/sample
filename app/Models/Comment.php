<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $table = "comments";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = ['comentario','estrellas','evento_id'];


   public function evento(){
        return $this->belongsTo('App\Models\Evento');
    }


}

