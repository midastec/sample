<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.groups';

    /**
     * Nombre que se le dio al primary key del modelo
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Permite activar o desactivar
     * la insercion  o actualizacion en los campos
     * update, create que tiene por defecto laravel
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name', 'description','creado_en','modificado_en'];

    /*
	* Relacion de los grupos y sus operaciones
    */
    public function operation()
    {
        return $this->belongsToMany('App\Models\OperationModel','acl.group_operation', 'id_group', 'id_operation');
    }

}
