<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Maykol Purica <puricamaykol@gmail.com>
 *          Class Balance
 * @package App\Models
 */
class Balance extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'balances';

    protected $fillable = [
        "cliente_id",
        "orden_publicacion_id",
        "saldo",
        "debito",
        "cargo",
        "fecha",
        "created_at",
        "contrato_id",
        "observaciones",
        "usuario_id",
    ];

    protected $hidden = [
        "updated_at",
        "deleted_at",
    ];
    /**
     * Relación con modelo de Contrato
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contrato(){
        return $this->belongsTo('App\Models\Contrato');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     * Esta funcion se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacio entonces ninguno lo sera
     */
    public function getDates()
    {
        return array();
    }

}
