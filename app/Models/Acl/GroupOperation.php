<?php namespace App\Models\Acl;

use Illuminate\Database\Eloquent\Model;

class GroupOperation extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.group_operation';


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_operation','id_group'];


    public function group()
    {
        return $this->hasMany('App\Models\Acl\Group','id_group');
    }
    public function operation()
    {
        return $this->hasMany('App\Models\Acl\Operation','id_operation');
    }

}