<?php namespace App\Models\Acl;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model {

    protected $table = 'acl.users_groups';

}
