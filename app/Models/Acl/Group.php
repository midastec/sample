<?php namespace App\Models\Acl;

//
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Group extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.groups';

    /**
     * Nombre que se le dio al primary key del modelo
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'created_by'];

    /**
	* Relación de los grupos y sus operaciones
    */
    public function operation()
    {
        return $this->belongsToMany('App\Models\Acl\Operation', 'acl.group_operation', 'id_group', 'id_operation');
    }

    /**
     * @param $id_group
     * @return mixed
     */
    public function groupOperationsIDs($id_group)
    {
        $resultArray = [];
        $operationsArray = DB::table('acl.group_operation')
            ->where('id_group', $id_group)
            ->select('id_operation')->get();
        foreach ($operationsArray as $key => $value) {
            $resultArray[$key] = $value->id_operation;
        }
        return $resultArray;
    }


}
