<?php namespace App\Models\Acl;
//
//use Hash;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.users';

    /**
     * The primary key  used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'idsecretquestion', 'answer', 'confirmation_code', 'lastlogin'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public $timestamps=false;

    /**
     * Relación de los usuarios y sus grupo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function group()
    {
        return $this->belongsToMany('App\Models\Acl\Group', 'acl.users_groups', 'user_id', 'group_id');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cotizaciones()
    {
        return $this->hasMany('App\Models\Cotizacion', 'usuario_id');
    }

    public function user_token()
    {
        return $this->hasMany('App\Models\UserTokenModel','usuario_id');
    }

    /**
     * @param $name
     * @param bool $only_id
     * @return mixed
     */
    public function findByName($name, $only_id = false)
    {
        $fields = ($only_id) ? ['id'] : ['*'];
        return $this->where('username', $name)->first($fields);
    }

    public static function verificarUsuariosSinValidar(){
        $users=self::whereHas('user_token', function($query){
            $query->where('validado' , '=' ,false);
        })->with('user_token')->get()->toArray();

        foreach($users as $user){
            $validado=null;
            foreach($user['user_token'] as $user_token){
                if(!isset($validado)){
                    $validado=$user_token['validado'];
                }
            }

            $fechaActual = Carbon::now();
            if($validado == false){
                if(strtotime($fechaActual->toDateTimeString()) < strtotime('+3 month', strtotime($user_token['fechacrea']))){
                    $user=User::find($user['id']);
                    $user->active =0;
                    $user->modificado_en =Carbon::now();
                    $user->save();
                }
            }
        }
    }
}
