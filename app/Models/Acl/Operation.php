<?php
namespace App\Models\Acl;

//
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Operation extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.operation';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', '_name', 'url', '_order', 'visible', 'icon', 'help', 'tooltip', 'id_operation', 'chk_render_on', 'chk_visual_type', 'chk_target_on'];

    /**
     * Devuelve todos los iconos disponibles para las operaciones.
     *
     * @return array
     * @author Jose Rodriguez
     */
    public function getIcons()
    {
        return array_merge($this->getFontAsomewareIcons(), $this->getBootstrapIcons());
    }

    /**
     * Obtiene los iconos del archivo sass de Font-asomeware
     *
     * @return array
     * @author Jose Rodriguez
     */
    private function getFontAsomewareIcons()
    {
        // Fetch variables scss file in variable
        $font_vars = file_get_contents(base_path() . '/../app/js/vendor/font-awesome/scss/_variables.scss');

        // Find the position of first $fa-var , as all icon class names start after that
        $pos = strpos($font_vars, '$fa-var');

        // Filter the string and return only the icon class names
        $font_vars = substr($font_vars, $pos);

        // Create an array of lines
        $lines = explode("\n", $font_vars);


        $fonts_list = array();
        foreach ($lines as $line) {
            // Discard any black line or anything without :
            if (strpos($line, ':') !== false) {
                // Icon names and their Unicode Private Use Area values are separated with : and hence, explode them.
                $t = explode(":", $line);

                // Remove the $fa-var with fa, to use as css class names.
                //$fonts_list[str_replace('$fa-var', 'fa', $t[0])] = $t[1];
                $fonts_list[] = str_replace('$fa-var', 'fa fa', $t[0]);
            }
        }

        return $fonts_list;
    }

    /**
     * Obtiene los iconos del archivo less de Bootstrap
     *
     * @return array
     * @author Jose Rodriguez
     */
    private function getBootstrapIcons()
    {
        // Fetch variables scss file in variable
        $glyphicon_vars = file_get_contents(base_path() . '/../app/js/vendor/bootstrap/less/glyphicons.less');
        $lines = explode("\n", $glyphicon_vars);
        $glyphicons = [];
        foreach ($lines as $line) {
            if (empty($line) || strpos($line, '.glyphicon-') === false)
                continue;

            list($glyphicon) = explode('{', $line);
            $glyphicon = 'glyphicon ' . trim(trim($glyphicon), '.');
            array_push($glyphicons, $glyphicon);
        }

        return $glyphicons;
    }

    /**
     * Contiene el arreglo de valores existente en el check de render_on.
     * lastima no puedo leerlo de postgresl.
     *
     * @return array
     */
    public function getRenders()
    {
        return ['grid', 'sidebar', 'topbar', 'actionbar', 'button', 'tabpanel'];
    }

    /**
     * Contiene el arreglo de valores existente en el check de target_on.
     * lastima no puedo leerlo de postgresl.
     *
     * @return array
     */
    public function getTargets()
    {
        return ['content', 'tab', 'tab_content', 'window', 'window2'];
    }

    /**
     * Contiene el arreglo de valores existente en el check de visual_types.
     * lastima no puedo leerlo de postgresl.
     *
     * @return array
     */
    public function getVisuals()
    {
        return ['menu', 'submenu', 'method_js', 'link'];
    }

    /**
     * Obtiene solo el id y el nombre de todas las operaciones registradas.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOptions()
    {
        return parent::all(['id', '_name']);
    }


    /**
     * @param array $operation
     * @return static
     */
    public function crear($operation)
    {
        return parent::create($operation);
    }

    /**
     * Borrar Operación
     *
     * @param $id
     * @return int
     */
    public function borrar($id)
    {
        DB::table('acl.group_operation')->where('id_operation', '=', $id)->delete();
        return parent::destroy($id);
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Acl\Operation', 'id_operation');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Acl\Operation', 'id_operation');
    }

    public static function operationsMenu()
    {
        $menu = DB::select(DB::raw("WITH RECURSIVE q AS
                                    (
                                        SELECT
                                                distinct h,
                                                1 AS level,
                                                ARRAY[h.id] AS breadcrumb
                                        FROM
						acl.users_groups as user_g, acl.operation as h
                                        WHERE

                                                h.id_operation IS NULL        UNION ALL
                                        SELECT
                                                distinct hi,
                                                q.level + 1 AS level,
                                                breadcrumb || hi.id
                                        FROM
                                                q
                                                JOIN acl.operation hi ON hi.id_operation = (q.h).id
)
                SELECT
                        REPEAT('', level) || (q.h).id as id,
                        REPEAT('', level) || (q.h).id as value,
                        COALESCE((q.h).id_operation, 0) as id_operation,
                        (CASE WHEN (q.h).id_operation IS NULL THEN
                                                    upper ((q.h)._name)
                                              ELSE
                                                    (q.h)._name
                                              END) as label,
                        level,
                        breadcrumb::VARCHAR AS path
                FROM
                        q
                ORDER BY
                        breadcrumb"));
        foreach ($menu as $key => $value) {
            $menuOutput[$key] = [
                'id' => $value->id,
                'value' => $value->value,
                'id_operation' => $value->id_operation,
                'label' => $value->label,
                'level' => $value->level,
                'path' => $value->path,
            ];
        }

        return $menuOutput;
    }

    public function insertGroupOperations($permisos_array, $grupo_id)
    {
        $operationsResponse = [];
        foreach ($permisos_array as $key => $value) {
            array_push($operationsResponse, ['id_group' => $grupo_id, 'id_operation' => $value]);
        }
        return $operationsResponse;

    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $operations
     * @param $arregloFinal
     * @param $group_id
     */
    public static function getOperationsArray($operations, &$arregloFinal, $group_id)
    {
        foreach ($operations as $key => $value):
            if (!empty($value['selected']) && $value['selected'] == true):
                $arregloFinal[] = $value['id'];
            endif;
            if (isset($value['children'])):
                self::getOperationsArray($value['children'], $arregloFinal, $group_id);
            endif;
        endforeach;
    }

    public function group()
    {
        return $this->belongsToMany('App\Models\Acl\Group', 'acl.group_operation', 'id_operation', 'id_group');
    }
}
