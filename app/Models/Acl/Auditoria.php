<?php namespace App\Models\Acl;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Jenssegers\Agent\Facades\Agent;
use yajra\Datatables\Datatables;

class Auditoria extends Model
{

    // Nombre de la tabla física en la base de datos
    protected $table = "acl.auditorias";

//     Atributos que se pueden asignar de manera masiva.
    protected $fillable =
        [
            'id',
            'usuario_id',
            'esquema',
            'tabla',
            'registro_id',
            'fecha',
            'transaccion',
            'datos_previos',
            'datos_actuales',
            'info_adicional'
        ];

//     Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [];

//     Desactivo los $timestamps (updated_at, created_at)
    public $timestamps = false;

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }


    /**
     * Registra el acceso del usuario en la tabla auditoria.
     * @return static
     */
    public function registrarAcceso()
    {
        return parent::create($this->prepareInsert());
    }

    /**
     * Determina el tipo de acceso para la auditoria. Si se pasa evento, determina la transacción en base al
     * evento indicado.
     * @param string $evento
     * @return string
     */
    private function determinarTransaccion($evento = '')
    {
        $transaccion = '';
        $evento = (empty($evento)) ? Request::method() : $evento;
        switch ($evento) {
            case 'POST':
            case 'creating':
            case 'created':
                $transaccion = 'INSERT';
                break;

            case 'PUT':
            case 'PATCH':
            case 'updated':
            case 'updating':
                $transaccion = 'UPDATE';
                break;

            case 'DELETE':
            case 'deleting':
            case 'deleted':
                $transaccion = 'DELETE';
                break;

            case 'GET':
            default:
                $transaccion = 'ACCESS';
                break;
        }
        return $transaccion;
    }

    /**
     * Obtiene la información del referente al Cliente
     * @return string
     */
    private function getClientInformation(){
        $info_adicional = [
            'api' => 'Laravel 5',
            //'access_time' => date('Y-m-d H:i:s'),
            //'usuario_id' => '',
            'ip' => Request::ip(),
            'url' => Request::url(),
            'os' => Agent::platform(),
            'browser' => Agent::browser(),
            '_version' => Agent::version(Agent::browser()),
        ];
        return json_encode($info_adicional);
    }

    /**
     * Prepara los datos para una operación de acceso.
     * @return mixed
     */
    private function prepareInsert()
    {
        $user = new User();
        $data_user=$user->findByName(Request::header('username'), true);
        $data['usuario_id'] = (!empty($data_user)?$data_user->id:0);
        $data['fecha'] = date('Y-m-d H:i:s');
        $data['transaccion'] = $this->determinarTransaccion();
        $data['info_adicional'] = $this->getClientInformation();
        return $data;

    }

    /**
     * Actualiza la información generada por el trigger de auditoria.
     * @param $model
     * @param $trigger
     */
    public function actualizarAuditoria($model, $trigger){
        $schema = 'public';
        $table = $model->getTable();
        if(strpos($table,'.') !== false){
            list($schema, $table) = explode('.', $table);
        }

        $where = [
            'usuario_id' => $model->usuario_id,
            'esquema' => $schema,
            'tabla' => $table,
            'registro_id' => $model->id,
            'transaccion' => $this->determinarTransaccion($trigger)
        ];

        if($auditoria = $this->where($where)->whereNull('info_adicional')->orderBy('fecha', 'desc')->first()){
            $auditoria->info_adicional = $this->getClientInformation();
            $auditoria->save();
        }
    }

    /**
     * @author Amalio Velásquez
     * @version 17/05/2016 03:00 PM
     */
    public static function listar(){
        $auditorias=Auditoria::with('user.group');

        return Datatables::of($auditorias)
            ->make(true);
    }

    /**
     * @author Amalio Velásquez
     * @version 17/05/2016 03:03 PM
     * @updated 19/05/2016 10:30 AM
     */
    public static function ver($id){
        $auditoria=Auditoria::find($id);

        $auditoria->user->group;
        $auditoria->datos_previos=json_decode($auditoria->datos_previos);
        $auditoria->datos_actuales=json_decode($auditoria->datos_actuales);

        return $auditoria;
    }
}