<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacturaPagos extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.fact_pagos';

    protected $fillable = [
        'id',
        'usuario_id',
        'factura_id',
        'pago_id',
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }



}
