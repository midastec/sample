<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactoCliente extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'contactos_clientes';

    protected $fillable = [
        "nombre",
        "apellido",
        "telefonos",
        "correos_electronicos",
        "cargo_id",
        "es_interno",
        "cliente_id",
        "tipo",
        "descripcion",
        "usuario_id",
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
    ];

    /**
     * Relación con el modelo de Cargo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargo()
    {
        return $this->belongsTo('App\Models\Cargo');
    }


    /**
     * Relación con modelo de Cliente
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }
}
