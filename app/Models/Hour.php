<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Hour extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'municipio';

    protected $fillable = [
        "hour"
    ];

}
