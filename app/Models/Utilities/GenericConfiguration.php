<?php namespace App\Models\Utilities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class GenericConfiguration extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'utilities.generic_configuration';

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'id',
        'tipo_publicacion_id',
        'value',
        'usuario_id',
        'description',
        'validacion',
        'usuario_id',
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

       /** Relación con el modelo TipoPublicacion
       * @return \Illuminate\Database\Eloquent\Relations\HasMany
       */

    public function tipoPublicacion()
    {
        return $this->belongsTo('App\Models\TipoPublicacion');
    }

}
