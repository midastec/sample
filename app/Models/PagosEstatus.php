<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pagos;
use App\Models\Category;
use App\Models\EventoEstatus;
use Illuminate\Support\Facades\Mail;

class PagosEstatus extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.pagos_estatus';
    public $timestamps = false;
    protected $fillable = [
        'category_id',
        'pago_id'
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\UserModel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pago()
    {
        return $this->belongsTo('App\Models\Pagos');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function evento()
    {
        return $this->belongsTo('App\Models\Evento');
    }

    static function cambiar_estatus_pago($idPago, $request, $user)
    {
        $dataCreate = [
            "category_id" => $request->id,
            "pago_id" => $idPago,
            "usuario_id" => $user->id
        ];
        $response = PagosEstatus::create($dataCreate);
        if ($response) {
            $data = $data_pago = Pagos::detail_pago($idPago, true);

            //Envio de correos al cliente
            $data['APP_DESCRIPTION'] = getenv('APP_DESCRIPTION');
            $data['MAIL_USERNAME'] = getenv('MAIL_USERNAME');
            $data['APP_NAME'] = getenv('APP_NAME');
            $data['subject'] = 'Notificacion de Pagos';

            Mail::send(
                'alertaPagoCliente',
                array('data' => $data), function ($message) use ($data) {
                $message->to($data['persona']['email'], $data['persona']['nombre'] . ' ' . $data['persona']['apellido']);
                $message->from($data['MAIL_USERNAME'], $data['APP_NAME']);
                $message->subject($data['subject']);

            });
            //fin de envio de correos
            PagosEstatus::logica_eventoestatus_estatuspago($response->id);
            return $data_pago;
        }

        return false;
    }

    static function logica_eventoestatus_estatuspago($idPagoEstatus)
    {
        $data_pago = PagosEstatus::with(
            [
                'category',
                'pago',
                'pago.evento_pago'
            ]
        )->find($idPagoEstatus)->toArray();
        if ($data_pago):
            if ($data_pago['category']['_order'] == 3):
                $dataCreate = array();
                foreach ($data_pago['pago']['evento_pago'] as $key => $value):
                    if ($data_pago['category']['_order'] == 3):
                        $category_data = Category::where(array('_table' => 'estatus_evento', '_label' => 'Validado'))->get()->toArray();
                        $dataCreate = [
                            "category_id" => $category_data[0]['id'],
                            "evento_id" => $value['id'],
                            "usuario_id" => 1
                        ];
                    elseif ($data_pago['category']['_order'] == 1 || $data_pago['category']['_order'] == 2):
                        $category_data = Category::where(array('_table' => 'estatus_evento', '_label' => 'Por validar pago'))->get()->toArray();
                        $dataCreate = [
                            "category_id" => $category_data[0]['id'],
                            "evento_id" => $value['id'],
                            "usuario_id" => 1
                        ];


                    elseif ($data_pago['category']['_order'] == 1 || $data_pago['category']['_order'] == 2):
                        $category_data = Category::where(array('_table' => 'estatus_evento', '_label' => 'Por validar pago'))->get()->toArray();
                        $dataCreate = [
                            "category_id" => $category_data[0]['id'],
                            "evento_id" => $value['id'],
                            "usuario_id" => 1
                        ];


                    elseif ($data_pago['category']['_order'] == 4):
                        $category_data = Category::where(array('_table' => 'estatus_evento', '_label' => 'Por validar pago'))->get()->toArray();
                        $dataCreate = [
                            "category_id" => $category_data[0]['id'],
                            "evento_id" => $value['id'],
                            "usuario_id" => 1
                        ];
                    endif;
                    if (sizeof($dataCreate) > 0)
                        EventoEstatus::create($dataCreate);
                endforeach;
            endif;
        endif;
    }
}