<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model {


    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "rates";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = ['stars'];


}
