<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'estado';

    protected $fillable = [
        "nombre",
        "pais_id",
        "usuario_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    /**
     * Relación con el modelo Pais
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pais ()
    {
        return $this->belongsTo ('App\Models\Pais');
    }

    /**
     * Relación con el modelo Municipio
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function municipio ()
    {
        return $this->hasMany ('App\Models\Municipio','estado_id');
    }
}
