<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rutas extends Model {

	protected $table = 'rutas';
    public $timestamps = false;
     protected $fillable = [
            'evento_id',
  			'punto_inicio',
  			'punto_fin',
  			'ruta', 
    ];

    protected $hidden = [
       /* "created_at",
        "updated_at",
        "deleted_at",*/
    ];
    public function getDates()
    {
        return array();
    }
    public function evento(){
        return $this->belongsTo('App\Models\Evento');
    }

}
