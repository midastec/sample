<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecursosCategoria extends Model
{

    protected $table = 'public.recursos_categoria';

    public $timestamps = false;

    protected $fillable = [
        "category_id",
        "recurso_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    /**
     * Relación con el modelo de Persona
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoria()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function recurso()
    {
        return $this->belongsTo('App\Models\Recursos', 'recurso_id');
    }


}