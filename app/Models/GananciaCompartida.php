<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GananciaCompartida extends Model
{

    protected $table = 'ganancia_compartida';

    public $timestamps = false;

    protected $fillable = [
        'socio_id',
        'producto_id',
        'porcentaje_socio',
        'porcentaje_empres'
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];


}
