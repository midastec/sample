<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperationModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.operation';

    /**
     * Nombre que se le dio al primary key del modelo
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','_name','url','_order','visible','icon','help','tooltip','id_operation','chk_render_on','chk_visual_type','chk_target_on'];



/*
 *
 * JOSE
 *
 *
    public function submenu()
    {
        return $this->hasMany('App\Models\OperationModel','id_operation');
    }

    public function getMenu()
    {
        return $this->where('visible','=',1)->with(['submenu'])->get();
    }
*/


}
