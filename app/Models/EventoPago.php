<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use DateTime;
use Illuminate\Support\Facades\Validator;
use Exception;
use \HttpResponse;

class EventoPago extends Model
{
    protected $table = 'evento_pago';
    public $timestamps = false;
    protected $fillable = [
        'evento_id',
        'pago_id'
    ];


    public function getDates()
    {
        return array();
    }
    public function evento()
    {
        return $this->belongsTo('App\Models\Evento', 'evento_id');
    }

}
