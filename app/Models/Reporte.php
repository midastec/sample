<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PDO;

class Reporte extends Model
{

    private $cliente = '';
    protected $name = '';
    protected $sheet_name = '';
    private $extra_columns = array(); //Atributo que contendra las columnas extras a insertar al final de la hoja de excel

    public function __construct($name = '', $sheet_name = '')
    {
        parent::__construct();
        $this->name = $name;
        $this->sheet_name = $sheet_name;
    }

    /**
     * Funcion que trae la data de las publicaciones del cliente por id
     */
    public function reporteClientesDetallado($id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {

            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        } else {
            $datosPublicacion = Cliente::with('publicaciones')->find($id);

            return $datosPublicacion;
        }
    }

    /**
     *Funcion que trae la data de las publicaciones de todos los clientes
     *
     * @return Response
     */
    public function reporteClienteGeneral()
    {
        $datosPublicacion = Cliente::with('publicaciones')->get();

        return $datosPublicacion;

    }

    /**
     * Funcion que trae la data de los servicios creados del medio por id
     */
    public function reporteMediosDetallado($id)
    {
        $medio = Medio::find($id);
        if (!$medio) {

            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        } else {
            $datosServicios = Medio::with('servicios')->find($id);

            return $datosServicios;
        }
    }

    /**
     * * Funcion que trae la data de los servicios creados de todos los medio
     *
     * @return Response
     */
    public function reporteMedioGeneral()
    {
        $datosServicios = Medio::with('servicios')->get();

        return $datosServicios;

    }

    /**
     * @author Carlos Blanco
     */
    public function reporteClientesfiltros($cliente_id = null, $desde, $hasta)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $this->cliente = $cliente_id;


        if ($cliente_id == null) {
            if (!$desde == null) {

                // si la fecha no es nula muestra todos los registros en el rango dado

                $datosCliente['reporte_tipos'] = DB::select(DB::raw("
                WITH RECURSIVE reporte_tipo_publicaciones (cliente_id,cliente,web,impreso,radial,televisivo,vallas) AS (
        SELECT

            op.cliente_id,
            c.razon_social as cliente,
            (CASE WHEN opv.tipo_publicacion_name='Web' THEN count(opv.*) ELSE 0  END) AS web,
            (CASE WHEN opv.tipo_publicacion_name='Impreso' THEN count(opv.*) ELSE 0  END) AS impreso,
            (CASE WHEN opv.tipo_publicacion_name='Radial' THEN count(opv.*) ELSE 0  END) AS radial,
            (CASE WHEN opv.tipo_publicacion_name='Televisivo' THEN count(opv.*) ELSE 0 END) AS televisivo,
            (CASE WHEN opv.tipo_publicacion_name='Vallas' THEN count(opv.*) ELSE 0 END) AS vallas
        FROM
            ordenes_publicacion op
            INNER JOIN ordenes_publicacion_versiones opv ON op.id = opv.orden_publicacion_id
            inner join clientes c on op.cliente_id=c.id
        --

        WHERE
        --AQUI LA FECHA INICIAL
        --LAS DOS FECHAS TIENEN QUE RESPETAR EL FORMATO
        op.created_at BETWEEN '$desde'

        --AQUI LA FECHA FINAL
        --LAS DOS FECHAS TIENEN QUE RESPETAR EL FORMATO
        AND CAST('$hasta' AS DATE)+ CAST('1 days' AS INTERVAL)
        group by
        op.cliente_id,opv.tipo_publicacion_name,c.razon_social

        )
        SELECT
            cliente,
            sum(web) AS web,
            sum(impreso) AS impreso,
            sum(radial) AS radial,
            sum(televisivo) AS televisivo,
            sum(vallAS) AS vallas
        FROM
            reporte_tipo_publicaciones
        GROUP BY
           cliente"
                ));

                return $datosCliente['reporte_tipos'];

            }elseif($desde === null) {

                // si no tiene fecha muestra todos los registros

                $datosCliente['reporte_tipos_todos'] = DB::select (DB::raw ("
                WITH RECURSIVE reporte_tipo_publicaciones (cliente_id,cliente,web,impreso,radial,televisivo,vallas) AS (
        SELECT

            op.cliente_id,
            c.razon_social as cliente,
            (CASE WHEN opv.tipo_publicacion_name='Web' THEN count(opv.*) ELSE 0  END) AS web,
            (CASE WHEN opv.tipo_publicacion_name='Impreso' THEN count(opv.*) ELSE 0  END) AS impreso,
            (CASE WHEN opv.tipo_publicacion_name='Radial' THEN count(opv.*) ELSE 0  END) AS radial,
            (CASE WHEN opv.tipo_publicacion_name='Televisivo' THEN count(opv.*) ELSE 0 END) AS televisivo,
            (CASE WHEN opv.tipo_publicacion_name='Vallas' THEN count(opv.*) ELSE 0 END) AS vallas
        FROM
            ordenes_publicacion op
            INNER JOIN ordenes_publicacion_versiones opv ON op.id = opv.orden_publicacion_id
            inner join clientes c on op.cliente_id=c.id
        --
        )
        SELECT

            cliente,
            sum(web) AS web,
            sum(impreso) AS impreso,
            sum(radial) AS radial,
            sum(televisivo) AS televisivo,
            sum(vallAS) AS vallas
        FROM
            reporte_tipo_publicaciones
        GROUP BY
            cliente"
                ));

                return $datosCliente['reporte_tipos_todos'];
            }
        } else {

            if (!$desde == null) {

                //Si el cliente tiene un rango mostrara las publicaciones en ese rango

                $datosCliente = Cliente::whereHas('publicaciones', function ($q) {

                    $q->where('cliente_id', '=', $this->cliente);

                })->get();



                if ($datosCliente) {
                    $datosCliente['reporte_tipos'] = DB::select(DB::raw(

                    "
                WITH RECURSIVE reporte_tipo_publicaciones (cliente_id,cliente,web,impreso,radial,televisivo,vallas) AS (
                    SELECT

            op.cliente_id,
            c.razon_social as cliente,
            (CASE WHEN opv.tipo_publicacion_name='Web' THEN count(opv.*) ELSE 0  END) AS web,
            (CASE WHEN opv.tipo_publicacion_name='Impreso' THEN count(opv.*) ELSE 0  END) AS impreso,
            (CASE WHEN opv.tipo_publicacion_name='Radial' THEN count(opv.*) ELSE 0  END) AS radial,
            (CASE WHEN opv.tipo_publicacion_name='Televisivo' THEN count(opv.*) ELSE 0 END) AS televisivo,
            (CASE WHEN opv.tipo_publicacion_name='Vallas' THEN count(opv.*) ELSE 0 END) AS vallas
        FROM
            ordenes_publicacion op
            INNER JOIN ordenes_publicacion_versiones opv ON op.id = opv.orden_publicacion_id
            inner join clientes c on op.cliente_id=c.id
                        --

        WHERE
        --AQUI LA FECHA INICIAL
                    --LAS DOS FECHAS TIENEN QUE RESPETAR EL FORMATO
        op.created_at BETWEEN '$desde'

                    --AQUI LA FECHA FINAL
        --LAS DOS FECHAS TIENEN QUE RESPETAR EL FORMATO
                    AND CAST('$hasta' AS DATE)+ CAST('1 days' AS INTERVAL)

        group by
        op.cliente_id,opv.tipo_publicacion_name,c.razon_social

        )
        SELECT

            cliente,
            sum(web) AS web,
            sum(impreso) AS impreso,
            sum(radial) AS radial,
            sum(televisivo) AS televisivo,
            sum(vallAS) AS vallas
        FROM
            reporte_tipo_publicaciones
                          --WHERE
        --    cliente_id=AQUI EL ID DEL CLIENTE
         WHERE
                cliente_id=$cliente_id
        GROUP BY
            cliente"
                    ));
                }
             return   $datosCliente['reporte_tipos'];

            } elseif($desde == null) {

                //Si el cliente no tiene un rango muestra todas las publicaciones echas.

                $datosCliente = Cliente::whereHas('publicaciones', function ($q) {

                    $q->where('cliente_id', '=', $this->cliente);

                })->get();
                $datosCliente['reporte_tipos_todos'] = DB::select(DB::raw(

                      "
                WITH RECURSIVE reporte_tipo_publicaciones (cliente_id,cliente,web,impreso,radial,televisivo,vallas) AS (
                SELECT

            op.cliente_id,
            c.razon_social as cliente,
            (CASE WHEN opv.tipo_publicacion_name='Web' THEN count(opv.*) ELSE 0  END) AS web,
            (CASE WHEN opv.tipo_publicacion_name='Impreso' THEN count(opv.*) ELSE 0  END) AS impreso,
            (CASE WHEN opv.tipo_publicacion_name='Radial' THEN count(opv.*) ELSE 0  END) AS radial,
            (CASE WHEN opv.tipo_publicacion_name='Televisivo' THEN count(opv.*) ELSE 0 END) AS televisivo,
            (CASE WHEN opv.tipo_publicacion_name='Vallas' THEN count(opv.*) ELSE 0 END) AS vallas
            FROM
                ordenes_publicacion op
                INNER JOIN ordenes_publicacion_versiones opv ON op.id = opv.orden_publicacion_id
                inner join clientes c on op.cliente_id=c.id
                    --
        group by
        op.cliente_id,opv.tipo_publicacion_name,c.razon_social

        )
        SELECT

            cliente,
            sum(web) AS web,
            sum(impreso) AS impreso,
            sum(radial) AS radial,
            sum(televisivo) AS televisivo,
            sum(vallAS) AS vallas
        FROM
            reporte_tipo_publicaciones
            --WHERE
                --    cliente_id=AQUI EL ID DEL CLIENTE
         WHERE
                cliente_id=$cliente_id
        GROUP BY
            cliente"
                ));


                DB::setFetchMode(PDO::FETCH_CLASS);

                return $datosCliente['reporte_tipos_todos'];

            }
        }
    }

    /**
     * @param $filtros
     * @return mixed
     */
    public function getNumeroCotizaciones($filtros)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $report = DB::table('cotizaciones AS cot')
            ->join('clientes AS cli', 'cli.id', '=', 'cot.cliente_id')
            //->select(['cli.id', 'cli.rif', 'cli.razon_social', DB::raw('COUNT(cot.id) AS total_cotizaciones')])
            ->select(['cli.rif AS RIF', DB::raw('cli.razon_social AS "Razón Social"'), DB::raw('COUNT(cot.id) AS "Total Cotizaciones"')])
            ->groupBy(['cli.id', 'cli.rif', 'cli.razon_social']);

        $date = new Carbon();
        $start = (empty($filtros['dateStart'])) ? $date->createFromDate(2000, 01, 01) : $date->createFromFormat('Y-m-d', $filtros['dateStart']);
        $finish = (empty($filtros['dateEnd'])) ? $date->now() : $date->createFromFormat('Y-m-d', $filtros['dateEnd']);
        $report->whereBetween('cot.fecha_creacion', array($start, $finish));

        if (!empty($filtros['cliente_id']) && is_numeric($filtros['cliente_id']))
            $report->where('cot.cliente_id', '=', $filtros['cliente_id']);

        if (preg_grep('/medio_id|tipo_publicacion_id/i', array_keys($filtros))) {

            // Agrego el Join con versiones.
            $report->join('cotizaciones_servicios AS cser', function ($join) use ($filtros) {
                //cotizaciones_servicios(servicio_id,usuario_id,medio_id,tipo_publicacion_id);
                $join->on('cser.cotizacion_id', '=', 'cot.id');
                if (!empty($filtros['medio_id']) && is_numeric($filtros['medio_id'])) {
                    $join->on('cser.medio_id', '=', DB::raw($filtros['medio_id']));
                }
                if (!empty($filtros['tipo_publicacion_id']) && is_numeric($filtros['tipo_publicacion_id'])) {
                    $join->on('cser.tipo_publicacion_id', '=', DB::raw($filtros['tipo_publicacion_id']));
                }
            });
        }

        $report = $report->get();

        // of course to revert the fetch mode you need to set it again
        DB::setFetchMode(PDO::FETCH_CLASS);

        return $report;
    }

    /**
     * @return array
     */
    private function getHeaders()
    {
        $request = Request::capture();
        $filtros = array_merge($request->all(), $request->header());

     //   $user = (new User())->findByName($filtros['username'][0]);
        //$user = (new User())->findByName('administrator');

        $headers = [
            ['Fecha de Creación', date('d-m-Y H:i:s')],
          //  ['Creado por', "{$user['first_name']} {$user['last_name']}"]
        ];

        if (preg_grep('/medio_id|cliente_id|dateStart|dateEnd|tipo_publicacion_id/i', array_keys($filtros))) {
            array_push($headers, ['Filtros']);
            if (array_key_exists('cliente_id', $filtros) && !empty($filtros['cliente_id']) ) {
                $cliente = Cliente::find($filtros['cliente_id'], ['razon_social', 'codigo']); //,'nombre_comercial'
                array_push($headers, ['Cliente', "{$cliente->codigo} - {$cliente->razon_social}"]);
            }

            if (array_key_exists('tipo_publicacion_id', $filtros) && !empty($filtros['tipo_publicacion_id'])) {
                $tipo = TipoPublicacion::find($filtros['tipo_publicacion_id'], ['nombre']); //,'nombre_comercial'
                array_push($headers, ['Tipo de Publicación', $tipo->nombre]);
            }

            if (array_key_exists('medio_id', $filtros) && !empty($filtros['medio_id'])) {
                $medio = Medio::find($filtros['medio_id'], ['razon_social', 'codigo']); //,'nombre_comercial'
                array_push($headers, ['Proveedor', "{$medio->codigo} - {$medio->razon_social}"]);
            }

            if (array_key_exists('dateStart', $filtros) && !empty($filtros['dateStart'])) {
                array_push($headers, ['Desde', implode('-', array_reverse(explode('-', $filtros['dateStart'])))]);
            }

            if (array_key_exists('dateEnd', $filtros) && !empty($filtros['dateEnd'])) {
                array_push($headers, ['Hasta', implode('-', array_reverse(explode('-', $filtros['dateEnd'])))]);
            }
        }

        return $headers;
    }

//    public function build($request, $data){
    public function build($data, $extra = array())
    {
        $this->extra_columns = $extra;
        $headers = $this->getHeaders();
        Excel::create($this->name, function ($excel) use ($data, $headers) {
            $excel->sheet($this->sheet_name, function ($sheet) use ($data, $headers) {

                $init = count($headers) + 3;
                $sheet->rows($headers);
                $sheet->appendRow(['Total Registros', count($data)]);
                $sheet->fromArray($data, null, "A{$init}");
                $sheet->setAllBorders('thin');

                for ($i = 1, $len = count($headers) + 1; $i <= $len; $i++) {
                    $sheet->cell("A{$i}", function ($cell) {
                        $cell->setBackground('#D13138');
                        $cell->setFontColor('#ffffff');
                    });
                }

                $sheet->row($init, function ($row) {
                    // call cell manipulation methods
                    $row->setBackground('#D13138');
                    $row->setFontColor('#ffffff');
                    $row->setAlignment('center');

                    // Set font
                    $row->setFont([
                        'family' => 'Calibri',
                        'size' => '12',
                        'bold' => true,
                    ]);

                });
                //Esto inserta una fila al final de la hoja de excel en funcion de los valores pasados en un arreglo unidimensional
                $sheet->appendRow($this->extra_columns);
            });
        })->download('xlsx');
    }
    /**
     * * 
     *
     * @author Maykol Purica
     */
    public function facturasTotales($cliente_id = null, $desde = null, $hasta = null)
    {

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'SELECT cli.razon_social, fac.created_at, fac.codigo,
               fac.fecha_generada,
               fac.estatus, total_factura
          FROM factura_cliente fac, clientes cli, ordenes_publicacion ord_pub
        WHERE
            ord_pub.cliente_id = cli.id AND
            ord_pub.factura_id = fac.id';
        if ($cliente_id != null):
            $query .= ' AND cli.id = ' . $cliente_id;
        endif;
        if ($desde != null && $hasta != null):
            $query .= " AND fac.created_at BETWEEN '" . $desde . "' AND '" . $hasta . "'";
        endif;
        $query .= ' GROUP BY cli.razon_social, fac.created_at, fac.codigo,
               fac.fecha_generada,
               fac.estatus, total_factura';

        $factura = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $factura;
    }
    /**
     * * 
     *
     * @author Maykol Purica
     */
    public function facturasPagadas($cliente_id = null, $tipo_publicacion = null)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'SELECT fact.codigo as fact_codigo,fact.id as fact_id,cli.codigo as cli_codigo, cli.razon_social, fact.total_factura,
                 (fact.total_factura - fact.restante  ) as pagado
                FROM
                clientes cli, ordenes_publicacion pub, factura_cliente fact, ordenes_publicacion_versiones vers, tipos_publicaciones tipub
                WHERE
                pub.cliente_id = cli.id AND
                pub.factura_id = fact.id AND
                vers.tipo_publicacion_id = tipub.id AND
                vers.orden_publicacion_id = pub.id';
        if ($tipo_publicacion != null):
            $query .= ' AND vers.tipo_publicacion_id = ' . $tipo_publicacion;
        endif;

        if ($cliente_id != null):
            $query .= ' AND cli.id = ' . $cliente_id;
        endif;

        $query .= ' GROUP BY cli.id, cli.codigo, cli.razon_social, fact.codigo, fact.total_factura, fact.restante,fact.id ';
        $factura = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $factura;
    }
    /**
     * * 
     *
     * @author Maykol Purica
     */
    public function incidencias($cliente_id = null, $desde = null, $hasta = null)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'SELECT pub.codigo as codigo_pub, tipo_inci.nombre as tipo_incidencia, cli.razon_social
            FROM ordenes_publicacion_incidencias inci, ordenes_publicacion pub, clientes cli, tipos_incidencias tipo_inci
            WHERE
                pub.id = inci.orden_publicacion_id AND
                cli.id = pub.cliente_id AND
                inci.tipo_incidencia_id = tipo_inci.id AND
                pub.cliente_id = cli.id';
        if ($cliente_id != null):
            $query .= ' AND cli.id = ' . $cliente_id;
        endif;

        if ($desde != null && $hasta != null):
            $query .= " AND inci.created_at BETWEEN '" . $desde . "' AND '" . $hasta . "'";
        endif;
        $incidencia = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $incidencia;
    }


    /**
     * @author Carlos Blanco
     */
    public function montoCobrado($cliente_id = null, $desde = null, $hasta = null)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'SELECT cli.razon_social as cliente, pago.created_at as fecha_pago, pago.observaciones ,pago.monto
                    FROM
                       factura_cliente fc

                    INNER JOIN fact_pago fp on fc.id=fp.factura_id
                    INNER JOIN pagos pago on pago.id = fp.pago_id
                    INNER JOIN ordenes_publicacion op on  fc.id = op.factura_id
                    INNER JOIN clientes cli on cli.id=op.cliente_id

                      ';
        if ($cliente_id != null):
            $query .= ' WHERE cli.id = ' . $cliente_id;
        endif;

        if ($desde != null && $hasta != null):
            $query .= " AND pago.created_at BETWEEN '" . $desde . "' AND '" . $hasta . "'";
        endif;
        $query .= ' GROUP BY cli.razon_social, pago.created_at,  pago.observaciones, pago.monto ';
        $datapagada = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $datapagada;
    }
    /**
     * * 
     *
     * @author Maykol Purica
     */
    public function pautaIncidencia($cliente_id = null, $medio= null, $tipo_publicacion = null,$desde = null, $hasta = null)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'SELECT cli.razon_social as Cliente, med.razon_social as Medio, pub.codigo as Publicacion, pub.fecha_creacion as fecha_publicacion,
         tipo_inci.nombre as Incidencia, inci.created_at as fecha_incidencia 
        FROM
            ordenes_publicacion pub, ordenes_publicacion_incidencias inci, tipos_incidencias tipo_inci, medios med, clientes cli
        WHERE
         pub.medio_id = med.id AND
         pub.cliente_id = cli.id AND
         inci.orden_publicacion_id = pub.id AND
         inci.tipo_incidencia_id = tipo_inci.id';
        if ($cliente_id != null):
            $query .= ' AND cli.id = ' . $cliente_id;
        endif;
        if ($tipo_publicacion != null):
            $query .= ' AND tipo_inci.tipo_publicacion_id = ' . $tipo_publicacion;
        endif;
        if ($medio != null):
            $query .= ' AND med.id = ' . $medio;
        endif;
        if ($desde != null && $hasta != null):
            $query .= " AND inci.created_at BETWEEN '" . $desde . "' AND '" . $hasta . "'";
        endif;
        
        $incidencia = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $incidencia;
    }
    /**
     * * 
     *
     * @author Maykol Purica
     */
     public function contratadoPagado($cliente_id = null, $desde = null, $hasta = null)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'SELECT 
            cli.razon_social as cliente, cont.codigo as contrato, cont.cuota as monto_contratado,pub.codigo as publicacion ,pub.total as monto_consumido
            FROM
             contratos cont, ordenes_publicacion pub, cotizaciones cot, clientes cli
             WHERE
             cont.cliente_id = cli.id AND
             pub.cotizacion_id = cot.id AND
             pub.cliente_id = cli.id AND
             cont.activo = true';
        if ($cliente_id != null):
            $query .= ' AND cli.id = ' . $cliente_id;
        endif;
        if ($desde != null AND $hasta != null):
            $query .= " AND pub.created_at BETWEEN '" . $desde . "' AND '" . $hasta . "'";
        endif;
        
        $incidencia = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $incidencia;
    }

    /**
     * @author Carlos Blanco
     */
	public function cotizacionesAprobadas($cliente_id = null, $desde = null, $hasta = null)
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = 'select
                   cli.razon_social as cliente, cot.codigo as cotizacion,op.codigo as codigo_publicacion,  opv.version_nombre
                    ,fact.total_factura

                    from

                    ordenes_publicacion op

                    inner join  cotizaciones cot on cot.id = op.cotizacion_id
                    inner join clientes cli  on cli.id = op.cliente_id
                    inner join  ordenes_publicacion_versiones opv on opv.orden_publicacion_id = op.id
                    inner join factura_cliente fact on fact.id = op.factura_id
                    where
                    cot.aprobada = true
                    ';
        if ($cliente_id != null):
            $query .= ' WHERE cli.id = ' . $cliente_id;
        endif;

        if ($desde != null && $hasta != null):
            $query .= " AND cot.created_at BETWEEN '" . $desde . "' AND '" . $hasta . "'";
        endif;
        $query .= ' GROUP BY cli.razon_social, cot.codigo, op.codigo,  opv.version_nombre, fact.total_factura';
        $cotaprobada = DB::select(DB::raw($query));
        DB::setFetchMode(PDO::FETCH_CLASS);
        return $cotaprobada;
    }
}