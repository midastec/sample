<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Maykol Purica <puricamaykol@gmail.com>
 *          Class Balance
 * @package App\Models
 */
class Ubicacion extends Model {

	/**
	 * Nombre de la tabla física en la base de datos
	 * @var string
	 */
	protected $table = 'ubicacion';

	protected $fillable = [
		"coordenadas",
		"evento_id",
		"device_id",
	];

	protected $hidden = [
		"updated_at",
		"deleted_at",
	];

	/**
	 * @author Maykol Purica <puricamaykol@gmail.com>
	 * @return array
	 * Esta funcion se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
	 * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
	 * Si se retorna el array vacio entonces ninguno lo sera
	 */
	public function getDates()
	{
		return array();
	}

}
