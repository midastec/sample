<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventoRecurso extends Model
{
    protected $table = 'public.eventos_recursos';

    public $timestamps = false;

    protected $fillable = [
        "recurso_id",
        "evento_id",
        "created_at",
        "porcentaje_socio",
        "producto_contratado_id"
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Relación con el modelo de Recursos
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recursos()
    {
        return $this->belongsTo('App\Models\Recursos', 'recurso_id');
    }

    /**
     * Relación con el modelo de ProductoContratado
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto_contratado()
    {
        return $this->belongsTo('App\Models\ProductoContratado', 'producto_contratado_id');
    }

    /**
     * Relación con el modelo de Eventos
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eventos()
    {
        return $this->belongsTo('App\Models\Evento', 'evento_id');
    }
}
