<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryCategory extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "category_category";

    public $timestamps = false;
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'id_parent',
        'id_child'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category_child()
    {
        return $this->belongsTo('App\Models\Category', 'id_child');
    }

    public function category_parent()
    {
        return $this->belongsTo('App\Models\Category', 'id_parent');
    }

}
