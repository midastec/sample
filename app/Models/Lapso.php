<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lapso extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "lapsos";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = ['lapso'];

}
