<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class EventoEstatus extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.evento_estatus';
    public $timestamps = false;
    protected $fillable = [
        'category_id',
        'evento_id'
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $dates = [
        'created_at'
    ];

    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\UserModel');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function evento()
    {
        return $this->belongsTo('App\Models\Evento');
    }



}
