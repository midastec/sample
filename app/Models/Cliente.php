    <?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cliente extends Model
{

    /**
     * Permite activar o desactivar
     * la insercion  o actualizacion en los campos
     * update, create que tiene por defecto laravel
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "persona";
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'id',
        'rif',
        'ci',
        'nombre',
        'apellido',
        'tipo_persona_id',
        'telefono',
        'email'
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [];



}
