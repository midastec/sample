<?php namespace App\Models;

use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Mail;
use App\Models\GroupModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Persona;
use App\Models\Category;
use Exception;

class UserModel extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.users';

    /**
     * The primary key  used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Permite activar o desactivar
     * la insercion  o actualizacion en los campos
     * update, create que tiene por defecto laravel
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'username',
        'first_name',
        'last_name',
        'email',
        'direccion',
        'phone',
        'cedula',
        'password',
        'creado_en',
        'last_login',
        'created_on',
        'active',
        'activation_code',
        'persona_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /*
    * Relacion de los usuarios y sus grupo
    */
    public function group()
    {
        return $this->belongsToMany('App\Models\GroupModel', 'acl.users_groups', 'user_id', 'group_id');
    }

    public function persona()
    {
        return $this->belongsTo('App\Models\Persona', 'persona_id');
    }

    /*
    * Relacion de los usuarios y sus tokens
    */
    public function token()
    {
        return $this->hasMany('App\Models\UserTokenModel', 'usuario_id');
    }

    /**
     * Provee los datos de un usuario especifico
     *
     * @access static
     * @param integer $id Identificador del Usuario
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 10:46 AM
     */
    static function get_usuario($id)
    {
        if (empty($id))
            return response()->json(null, 200);

        $data['groups'] = GroupModel::all();
        $user = new UserModel();
        $data['info_user'] = $user->with([
            'group'
        ])->find($id);

        if ($data['info_user'])
            $data['group_selected'] = $data['info_user']['group'][0];

        $data['info_user'] = $user->encript_decrypt_datos(
            $data['info_user']->toArray(), //Datos a procesar
            'decrypt',//Modo que aplicara
            array(
                'first_name',
                'last_name',
                'direccion',
                'phone',
                'cedula'
            ),//Claves que se le aplicara encriptacion
            false
        );

        return response()->json($data, 200);
    }


    /**
     * Permite actualizar los datos del usuario
     *
     * @access static
     * @param object $request Objeto de datos para procesar
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 10:49 AM
     */
    static function update_usuario($idUsuario, $request, $con_grupo = true)
    {
        $data = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'direccion' => (isset($request->direccion) ? $request->direccion : null),
            'phone' => $request->phone,
            'cedula' => $request->cedula,
            'group' => ($con_grupo ? $request->group : null),
        );
        $validacion = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'cedula' => 'required',
            'group' => 'required',
        ];
        $checkedMail = self::where('email', '=', $request->email)->pluck('email');
        /*        if ($checkedMail):
                    throw new Exception('El correo electronico suministrado ya se encuentra asociado a un usuario del sistema.');
                endif;*/
        if (!$con_grupo):
            unset($validacion['group']);
        endif;

        $v = Validator::make($data, $validacion);
        if ($v->fails()) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()]))], 422);
        }
        try {
            unset($data['group']);
            $user_object = new UserModel();
            $user = $user_object->with([
                'group'
            ])->find($idUsuario);
            if ($user):
                //Encriptacion de datos de usuario
                $data = $user_object->encript_decrypt_datos(
                    $data, //Datos a procesar
                    'encrypt',//Modo que aplicara
                    array(
                        'first_name',
                        'last_name',
                        'direccion',
                        'phone',
                        'cedula'
                    )//Claves que se le aplicara encriptacion
                );
                foreach ($data as $key => $value):
                    $user->{$key} = $data[$key];
                endforeach;
                if ($con_grupo)
                    $user->group()->detach(array($user->group[0]['id']));

                if ($user->save())
                    if ($con_grupo)
                        $user->group()->attach(array($request->group));
            endif;
        } catch (Exception $e) {
            return Response(array('success' => false, 'messages' => 'Error'), HttpResponse::HTTP_CONFLICT);

        }
        return Response(array('success' => true, 'messages' => 'Usuario actualizado exitosamente!', 'data' => $user), 200);

    }

    /**
     * Permite crear un usuario con sus datos encriptados
     *
     * @access static
     * @param object $request Objeto de datos a procesar
     * @param boolean $validacion_usuario Permite validar la cuenta del usuario mediante email
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 10:50 AM
     */
    static function create_usuario($request, $validacion_usuario = false, $notifica_password = false, $password_auto = false, $group_id = null)
    {
        $group = new GroupModel();

        $id_group_cliente = $group->where('name', '=', $request->tipo_persona)->get()->toArray();
        $id_group_cliente = ($id_group_cliente ? $id_group_cliente[0]['id'] : null);


        $password_auto_generado = strtolower(substr(str_replace(" ", "", $request->first_name), 0, 1) . str_replace(" ", "", $request->last_name));

        $_email = $request->email;
        $data = array(
            'username' => $_email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $_email,
            'direccion' => $request->direccion,
            'phone' => $request->phone,
            'cedula' => $request->cedula,
            'password' => ($validacion_usuario ? ($request->password ? bcrypt($request->password) : bcrypt($password_auto_generado)) : bcrypt($password_auto_generado)),
            'creado_en' => date('Y-m-d h:i:s'),
            'group' => ($validacion_usuario ? $id_group_cliente : $request->group),
            'created_on' => (int)strtotime(date('Y-m-d')),
            'active' => ($validacion_usuario ? 0 : 1),
            'persona_id' => ($request->persona_id ? $request->persona_id : null)

        );

        $v = Validator::make($data, [
            'username' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            //'direccion' => 'required',
            'phone' => 'required',
            'cedula' => 'required',
            'password' => 'required',
            'creado_en' => 'required',
            'group' => 'required',
            'active' => 'required'
        ]);
        if ($v->fails()) {
            $response = ['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])];
            $code = 422;
            return array('response' => $response, 'code' => $code);
        }


        try {
            $user_object = new UserModel();
            //Verificacion si el usuario existe

            if ($user_object->where('username', '=', $_email)->exists()):
                $response = array('success' => false, 'messages' => 'El usuario ya se encuentra registrado en el sistema!');
                $code = 202;
                return array('response' => $response, 'code' => $code);
            endif;

            $user_object2 = new UserModel();
            $datos = Persona::all()->lists('ci');
            $data2 = $user_object2->encript_decrypt_datos(
                $datos, 'decrypt', array(
                'ci',
            ), true
            );

           foreach($data2 as $da){
               if($da == $request->cedula){
                   $response = array('success' => false, 'messages' => 'El número de cédula ya se encuentra registrado en el sistema!');
                   $code = 202;
                   return array('response' => $response, 'code' => $code);
               }
           }

            unset($data['group']);

            $encript_key = [ 'nombre', 'apellido', 'ci', 'email', 'telefono' ];
            $personaData = [
                'nombre' => $request->input('first_name'),
                'apellido' => $request->input('last_name'),
                'ci' => $request->input('cedula'),
                'email' => $_email,
                'telefono' => $request->input('phone'),
                'tipo_persona_id' => Category::where('_table', '=', 'tipo_persona')
                    ->where('_label', '=', $request->tipo_persona)
                    ->pluck('id'),
            ];

            if($request->has('rif')){
                $personaData['rif'] = $request->input('rif');
                $encript_key[] = 'rif';
            }

            $personaData = UserModel::encript_decrypt_datos($personaData, 'encrypt', $encript_key);
            $persona = Persona::create($personaData);

            $data['persona_id'] = $persona->id;
            //Encriptación de datos de usuario
            $data_insert = $user_object->encript_decrypt_datos(
                $data, //Datos a procesar
                'encrypt',//Modo que aplicará
                [ 'first_name', 'last_name', 'direccion', 'phone', 'cedula' ]//Claves que se le aplicará encriptación
            );
            $user = $user_object->create($data_insert);
            if ($user):
                $user->group()->attach(($validacion_usuario ? array($id_group_cliente) : array($request->group)));
                if ($validacion_usuario) {
                    $new_token = str_random(150);
                    $token = new UserTokenModel(['token' => $new_token]);
                    if ($user->token()->save($token)) {
                        $data['token'] = $new_token;
                        $data['password_view'] = $password_auto_generado;
                        $user_object->email_confirmar_registro(array('usuario' => $data), false, $notifica_password);

                    }
                }
                $response = array('success' => true, 'messages' => 'Usuario creado exitosamente. Recibirá un correo electrónico para validar su dirección de correo. Debe seguir los pasos expuestos en el mismo.', 'data' => $data);
                $code = 200;
                return array('response' => $response, 'code' => $code);
            endif;


            $response = array('success' => false, 'messages' => 'Error al crear el registro!');
            $code = 201;
            return array('response' => $response, 'code' => $code);
        } catch (Exception $e) {
            $response = array('success' => false, 'messages' => $e->getMessage());
            $code = 201;//HttpResponse::HTTP_CONFLICT;
            return array('response' => $response, 'code' => $code);
        }


    }

    /**
     * Provee el listado de los usuario preparado para Datatables
     *
     * @access static
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 10:53 AM
     */
    static function dt_usuario()
    {
        $user_object = new UserModel();
        $query = $user_object->with('group')->get();
        $data['data'] = $user_object->encript_decrypt_datos(
            $query->toArray(), //Datos a procesar
            'decrypt',//Modo que aplicara
            ['first_name','last_name','direccion','phone','cedula']//Claves que se le aplicara encriptacion

        );
        $data = new Collection($data['data']);
        return Datatables::of($data)->make(true);
    }

    static function all_usuarios()
    {
        $user_object = new UserModel();
        $data['groups'] = $user_object->all();
        return response()->json($data, 200);

    }

    /**
     * Permite permite encriptar o desencriptar los valores de un arreglo dado
     *
     * @access static
     * @param array $datos Arreglo de datos a procesar
     * @param string $modo Establece si se encriptaran o se desencriptaran los datos
     * @param array $aplica_a Arreglo de claves que se le aplicara la encriptacion o desencriptacion con respecto
     *                        al arreglo dado ($datos)
     * @return array
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 10:55 AM
     */
    static function encript_decrypt_datos($datos = array(), $modo = '', $aplica_a = array())
    {
        $user_object = new UserModel();
        if (!empty($datos)) {
            foreach ($datos as $key => &$value):
                if (is_array($value)):
                    foreach ($value as $k => &$v):
                        if (in_array($k, $aplica_a)):
                            $v = ($modo == "encrypt" ? Crypt::encrypt($v) : ($modo == "decrypt" && !$user_object->invalidPayload($v) ? Crypt::decrypt($v) : $v));
                        endif;
                    endforeach;
                else:
                    if (in_array($key, $aplica_a)):
                        $value = ($modo == "encrypt" ? Crypt::encrypt($value) : ($modo == "decrypt" && !$user_object->invalidPayload($value) ? Crypt::decrypt($value) : $value));
                    endif;
                endif;
            endforeach;
        }
        return $datos;
    }

    /**
     * Verify that the encryption payload is valid.
     *
     * @param  array|mixed $payload
     * @return bool
     */
    static function invalidPayload($payload)
    {
        if (is_string($payload)):
            $data = json_decode(base64_decode($payload), true);
            return !is_array($data) || !isset($data['iv']) || !isset($data['value']) || !isset($data['mac']);
        else:
            return true;
        endif;
    }

    /**
     * Verifica si un token es valido para verificar la cuenta de un usuario o restablecer
     * su contrase#a
     *
     * @access static
     * @param string @token Token de validacion del usuario
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 11:01 AM
     */
    static function check_token($token, $reset_password = false)
    {
        $userToken = new UserTokenModel();
        if ($userToken->where('token', '=', $token)->exists()) {
            $userToken = $userToken->where('token', '=', $token)->get()->toArray();
            $usuario_id = $userToken[0]['usuario_id'];
            $token_id = $userToken[0]['id'];
            $datosVistaToken = array();
            $fechaActual = Carbon::now();
            //Istancia para actualizar datos del token a usado
            $token = UserTokenModel::find($token_id);
            $user = self::find($token->usuario_id);

            if (($userToken[0]['usado']) == 't'):
                $datosVistaToken['mensaje'] = 'Este token ha sido usado';
                $datosVistaToken['estatus'] = 'usado';

            else:
                $token->usado = true;
                $token->validado = true;
                if ($token->save()):
                    if ($reset_password):
                        $datosVistaToken['mensaje'] = 'Verificación de cambio de contraseña exitoso. Puedes proceder a cambiarla!';
                        $estatus = 'Activo';
                    else:
                        $user->active = true;
                        if ($user->save()):
                            $datosVistaToken['mensaje'] = 'Tu registro de usuario ha sido validado de forma exitosa! Puedes iniciar sesión!';
                            $estatus = 'Activo';
                        else:
                            $estatus = 'Fallido';
                            $datosVistaToken['mensaje'] = 'Hubo un error al validar tu cuenta!';
                        endif;
                    endif;
                    $datosVistaToken['estatus'] = $estatus;
                else:
                    $datosVistaToken['mensaje'] = 'No se Actualizo';
                    $datosVistaToken['estatus'] = 'Fallido';
                endif;
            endif;

            $user = UserModel::find($usuario_id);


            if (!empty($user)) {
                $data_usuario = UserModel::encript_decrypt_datos(
                    $user->toArray(), //Datos a procesar
                    'decrypt',//Modo que aplicara
                    ['first_name','last_name','direccion','phone','cedula']
                );
                $datosVistaToken['usuario'] = $data_usuario;
                $datosVistaToken['appName'] = getenv('APP_NAME');
                $datosVistaToken['appDescription'] = getenv('APP_DESCRIPTION');

                $datosVistaToken['url_inicio'] = getenv('DOMAIN') . getenv('APP_FRONTEND') .  getenv('APP_INDEX') . 'login';
                $datosVistaToken['css_1'] = getenv('DOMAIN') . getenv('APP_FRONTEND') .'css/app.css';
                $datosVistaToken['css_2'] = getenv('DOMAIN') . getenv('APP_FRONTEND') .'js/vendor/rdash-ui/dist/css/rdash.min.css';
                $datosVistaToken['css_3'] = getenv('DOMAIN') . getenv('APP_FRONTEND') .'css/rdash-ast.css';
                $url = 'auth0/apply_chance_password/';
                $datosVistaToken['form_url'] = url($url);
                $datosVistaToken['reset_password'] = $reset_password;
                $datosVistaToken['btn_name'] = ($reset_password && $datosVistaToken['estatus'] == 'Activo' ? 'Restablecer' : 'Inicio de Sesión');


                return View::make('ValidaToken')->with('data', $datosVistaToken);
            }
        }
    }

    /**
     * Envia un correo al usuario para la validacion de su cuenta
     *
     * @access static
     * @param array $data Arreglo de datos para procesar el email
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 11:06 AM
     */
    static function email_confirmar_registro($data = array(), $reset_password = false, $notifica_password = false)
    {
        if (empty($data))
            return false;


        $data['from'] = array(
            'email' => 'no-responder@disfruta-seguro.com',
            'nombre' => 'Sistema Disfruta Seguro'
        );

        $add_to_subject = ($reset_password ? 'Restablecer Contraseña' : 'Validacion de Registro');
        $data['subject'] = "Disfruta Seguro ($add_to_subject)";
        $data['reset_password'] = $reset_password;
        $data['notifica_password'] = $notifica_password;
        $url = ($reset_password ? 'auth0/chance_password/' : 'auth0/check/');
        $data['url'] = url($url . $data['usuario']['token']);
        $response = Mail::queue('EmailValidaClaveORegistro', array('data' => $data), function ($message) use ($data) {
            $message->to($data['usuario']['email'], $data['usuario']['first_name'] . " " . $data['usuario']['last_name']);
            $message->from($data['from']['email'], $data['from']['nombre']);
            $message->subject($data['subject']);
        });

        return $response;
    }

    /**
     * Genera el correo de confirmacion por token para la restablecer el password de un usuario
     *
     * @access static
     * @param object $request Objeto de datos a procesar
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 22/10/15 04:19 PM
     */
    static function solicitud_nuevo_password($request)
    {
        $user_object = new UserModel();
        if ($user_object->where('email', '=', $request->email)->exists()) {
            $user_data_array = $user_object->where('email', '=', $request->email)->get()->toArray();
            $user = UserModel::find($user_data_array[0]['id']);
            $new_token = str_random(150);
            $token = new UserTokenModel(['token' => $new_token]);
            if ($user->token()->save($token)) {
                $data = $user_data_array[0];

                $data = UserModel::encript_decrypt_datos(
                    $user->toArray(), //Datos a procesar
                    'decrypt',//Modo que aplicara
                    array(
                        'first_name',
                        'last_name',
                        'direccion',
                        'phone',
                        'cedula'
                    )
                );
                $data['token'] = $new_token;
                if ($user_object->email_confirmar_registro(array('usuario' => $data), true)) {
                    return Response(array('success' => true, 'messages' => 'Se ha enviado un correo de confirmacion a esta direccion para el reinicio de su contraseña!'), 200);
                } else {
                    return Response(array('success' => false, 'messages' => 'Ha ocurrido un error inesperado al enviar el correo de confirmacion a esta cuenta. Comuniquese con el Administrador!'), 201);
                }
            } else {
                return Response(array('success' => false, 'messages' => 'Ha ocurrido un error inesperado. Comuniquese con el Administrador!'), 201);
            }
        }
        return Response(array('success' => false, 'messages' => 'El correo electronico proporcionado no es valido en nuestro sistema!'), 201);
    }

    static function apply_chance_password($request)
    {
        $datosVistaToken['mensaje'] = '';
        $data = array(
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation
        );
        $v = Validator::make($data, [
            'email' => 'required',
            'password' => 'required|confirmed'
        ]);
        if ($v->fails()) {
            $datosVistaToken['mensaje'] = $v->errors();
        }

        $user_object = new UserModel();
        if ($user_object->where('email', '=', $request->email)->exists()) {
            $user_data_array = $user_object->where('email', '=', $request->email)->get()->toArray();
            $user = UserModel::find($user_data_array[0]['id']);
            $user->password = bcrypt($data['password']);
            if ($user->save() & $data['password'] == $data['password_confirmation']) {
                $datosVistaToken['mensaje'] = 'Se ha aplicado de manera exitosa!';
            } else {
                $datosVistaToken['mensaje'] = 'Las contraseñas no coinciden!!!';
            }
        } else {
            $datosVistaToken['mensaje'] = 'Usuario no existente en el sistema';
        }

        $datosVistaToken['appName'] = getenv('APP_NAME');
        $datosVistaToken['appDescription'] = getenv('APP_DESCRIPTION');

        $datosVistaToken['url_inicio'] = getenv('DOMAIN') . '/app/index.local.html#/login';
        $datosVistaToken['css_1'] = getenv('DOMAIN') . 'app/css/app.css';
        $datosVistaToken['css_2'] = getenv('DOMAIN') . 'app/js/vendor/rdash-ui/dist/css/rdash.min.css';
        $datosVistaToken['css_3'] = getenv('DOMAIN') . 'app/css/rdash-ast.css';
        $datosVistaToken['reset_password'] = true;
        $datosVistaToken['btn_name'] = 'Inicio de Sesion';
        $datosVistaToken['estatus'] = '';
        return View::make('ValidaToken')->with('data', $datosVistaToken);
    }

    static function io_user($id)
    {
        $response = array('success' => false, 'message' => 'Hubo un problema al aplicar los cambios.!');
        $code = 201;
        if (!empty($id)) {
            $user = UserModel::find($id);
            if ($user) {
                $user->active = ($user->active == 0 ? 1 : 0);
                $io_msg = ($user->active == 0 ? 'Desactivo' : 'Activo');
                if ($user->save()) {
                    $response = array('success' => true, 'message' => "El usuario se $io_msg exitosamente!");
                    $code = 200;
                } else {
                    $response = array('success' => false, 'message' => 'Hobo un problema al aplicar los cambios.!');
                    $code = 201;
                }
            } else {

                $response = array('success' => false, 'message' => 'Hobo un problema al aplicar los cambios.!');
                $code = 201;
            }
        }

        return Response($response, $code);
    }

}


