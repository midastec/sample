<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $casts = [ 'codigo' => 'string', ];
    protected $table = 'public.factura_cliente';
    /**
     * Campos que pueden ser llenados
     * @type array
     */
    protected $fillable = [
        'created_at',
        'codigo',
        'id',
        'updated_at',
        'nombre_firmante',
        'cargo_firmante',
        'fecha_motorizado',
        'fecha_cliente',
        'fecha_cancelada',
        'fecha_generada',
        'usuario_id',
        'total_factura',
        'restante',
    ];

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Relacion de Factura con Publicacion
     */
    public function publicacion()
    {
        return $this->hasMany('App\Models\Publicacion');
    }
    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     * Esta funcion se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacio entonces ninguno lo sera
     */
    public function getDates()
    {
        return array();
    }

    /**
     * Relación con Pagos.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTomany
     */
    public function pagos()
    {
        return $this->belongsToMany('App\Models\Pagos','public.fact_pago', 'factura_id','pago_id');
    }


}
