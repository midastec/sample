<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductoContratado extends Model
{
    protected $table = 'producto_contratado';

    public $timestamps = false;

    protected $fillable = [
        "descripcion",
        "costo_individual",
        "nombre",
        "image_path",
        "producto_id",
        "servicio_contratado_id",
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    public function evento()
    {
        return $this->belongsToMany('App\Models\Evento', 'evento_producto', 'producto_contratado_id', 'evento_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function producto(){
        return $this->belongsTo('App\Models\Producto');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param array $productoData
     * @return array
     * Metodo que trata de hallar el producto en la tabla de productos contratados, si lo encuentra
     * retorna su ID, sino, lo crea y retorna su ID
     */
    public function findOrCreateProductoContratado($productoData = array())
    {
        $producto = array();
        $producto = self::where('producto_id', '=', $productoData['id'])
            ->where('descripcion', '=', $productoData['descripcion'])
            ->where("costo_individual", "=", $productoData['costo_individual'])
            ->where("nombre", "=", $productoData['nombre'])
            ->where("image_path", "=", $productoData['image_path'])
            ->get()->toArray();
        if (count($producto) == 0):
            $nuevoProductoContratado = [
                'producto_id' => $productoData['id'],
                'descripcion' => $productoData['descripcion'],
                "costo_individual" => $productoData['costo_individual'],
                "nombre" => $productoData['nombre'],
                "image_path" => $productoData['image_path']
            ];
            $producto = self::create($nuevoProductoContratado);
            return array($producto->id => ['cantidad' => $productoData['cantidad']]);
        else:
            return array($producto[0]['id'] => ['cantidad' => $productoData['cantidad']]);

        endif;

    }

    /**
     * @return mixed
     * @author Jose Rodriguez
     */
    public function getTipoProducto()
    {
        return $this->producto()->first()->tipo_producto()->first();
    }

    /**
     * @return bool
     * @author Jose Rodriguez
     */
    public function isVehiculo()
    {
        return $this->getTipoProducto()->alt_value== 'vehiculo';
    }
}
